<? require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
set_time_limit(0);
define('CLIENT_ID', '65936');
define('TOKEN', '603a8fbd-64fd-4f15-ae9c-0743769b8785');
define('STOCK_PATH', 'http://marsgroup.ru/wiki/katalog_im_kr.xml');

$full_manuf_stocks = [67293, 67089, 67231];

if (!function_exists('nksSend')) {
	function nksSend($url, $header = [], $params = [], $type = 'GET')
	{   
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
		curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0); 
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		#curl_setopt($ch, CURLOPT_HEADER, 1);
		if (count($header)) {
			curl_setopt ($ch, CURLOPT_HTTPHEADER, $header);
		}
	
		if ($type == 'GET') {
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_TIMEOUT, 600);
			curl_setopt($ch, CURLOPT_HEADEROPT, CURLHEADER_UNIFIED);
			curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:53.0) Gecko/20100101 Firefox/53.0');
		}
		if ($type == 'POST') {
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		}
		if ($type == 'PUT') {
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
		}
		$result = curl_exec($ch);
		$code = curl_getinfo($ch,CURLINFO_HTTP_CODE);
		curl_close($ch);  
		return $result;
	}
}

function ozonGetProducts($page = 1, $page_size = 1000){
	$ids = [];
	$_list = nksSend(
		'http://api-seller.ozon.ru/v1/product/list',
		['Api-Key: '.TOKEN,'Content-Type: application/json;charset=utf-8', 'Client-Id: '.CLIENT_ID],
		json_encode([
			'filter' => [
				'visibility' => 'ALL'
			], 
			'page' => $page, 
			'page_size' => $page_size
		]),
		'POST'
	);

	$res = json_decode($_list, 1);

	if (!empty($res['result'])) {
		$ids = array_column($res['result']['items'],'offer_id');
		/*if ($res['result']['total'] > $page * $page_size) {
			$page++;				
			while ($r = ozonGetProducts($page)) {
				$ids = array_merge($ids, $r);					
			}
		}*/
		return $ids;
	} else {
		return false;
	}
}

$xml = simplexml_load_file(STOCK_PATH);
$json = json_encode($xml);
$loc_array = json_decode($json,TRUE);
$cur_list_ids = [];

foreach ($loc_array['supplier']['offers']['offer'] as $off) {
	$id = $off['vendorCode'];
	$rep = ["\t"," "," ",chr(9)];
	$cur_list_ids[$id]['stock'] = str_replace($rep,'',$off['stock']);
	if($cur_list_ids[$id] == "") {
		$cur_list_ids[$id]['stock'] = "0";
	}
	$cur_list_ids[$id]['name'] = $off['name'];
}

$ids = ozonGetProducts();
$ids = array_merge($ids, ozonGetProducts(2));

/* $_ids = file_get_contents(__DIR__.'/ids.txt');
$ids = json_decode($_ids, 1); */

$tmpIds = array_combine($ids, $ids);

$send = [];
foreach ($ids as $prodId) {
	if (array_key_exists($prodId, $cur_list_ids)) {
		$send[] = [
			"offer_id" => $prodId,
			"stock" => $cur_list_ids[$prodId]['stock'],
		];		
	}else{
		$send[] = [
			"offer_id" => $prodId,
			"stock" => 0,
		];
	}
}

$chunk = array_chunk($send,99);
$result = [];
foreach ($chunk as $k => $group) {
	$res = nksSend(
		'http://api-seller.ozon.ru/v1/product/import/stocks',
		['Api-Key: '.TOKEN,'Content-Type: application/json;charset=utf-8', 'Client-Id: '.CLIENT_ID],
		json_encode(['stocks' => $group]),
		'POST'
	);
	$result = array_merge($result, json_decode($res, 1));
}
$log = [];
foreach ($result['result'] as $key => $value) {
	if (!empty($value['errors'])) {
		$log[$value['offer_id']] = $value['errors'];
	}
}
file_put_contents(__DIR__.'/stock_krd_log.log',"================================\r\n" , FILE_APPEND);
file_put_contents(__DIR__.'/stock_krd_log.log', date('Y-m-d H:i:s')."\r\n" , FILE_APPEND);
file_put_contents(__DIR__.'/err_krd_log.log', print_r($log,1)."\r\n" , FILE_APPEND);
