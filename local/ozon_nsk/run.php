<?php
require(__DIR__ . "/config/config.php");
require(__DIR__ . "/classes/source.php");

if (! file_exists(__DIR__ . "/logs")) {
    mkdir(__DIR__ . "/logs");
}

if (! file_exists(__DIR__ . "/files")) {
    mkdir(__DIR__ . "/files");
}

if (file_exists(__DIR__ . "/logs/sync.log")) {
    $config['date_from'] = file_get_contents(__DIR__ . "/logs/sync.log");
}

$ozon = new Ozon($config);
$ozon->run();