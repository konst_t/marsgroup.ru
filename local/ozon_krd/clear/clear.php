<? require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

CModule::IncludeModule('iplogic.ozon');
CModule::IncludeModule('catalog');
CModule::IncludeModule('iblock');

$full_manuf_stocks = [67293, 67089, 67231];

$GLOBALS['api_key'] = 'a96f1d9a-1bcb-40ac-b54d-ba4e498a2507';
$GLOBALS['client_id'] = 74182;

if (!function_exists('nksSend')) {
	function nksSend($url, $header = [], $params = [], $type = 'GET')
	{   
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
		curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0); 
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		#curl_setopt($ch, CURLOPT_HEADER, 1);
		if (count($header)) {
			curl_setopt ($ch, CURLOPT_HTTPHEADER, $header);
		}
	
		if ($type == 'GET') {
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_TIMEOUT, 600);
			curl_setopt($ch, CURLOPT_HEADEROPT, CURLHEADER_UNIFIED);
			curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:53.0) Gecko/20100101 Firefox/53.0');
		}
		if ($type == 'POST') {
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		}
		if ($type == 'PUT') {
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
		}
		$result = curl_exec($ch);
		$code = curl_getinfo($ch,CURLINFO_HTTP_CODE);
		curl_close($ch);  
		return $result;
	}
}

function ozonGetProducts($page = 1, $page_size = 1000){
	$ids = [];
	$_list = nksSend(
		'http://api-seller.ozon.ru/v1/product/list',
		['Api-Key: '.$GLOBALS['api_key'],'Content-Type: application/json;charset=utf-8', 'Client-Id: '.$GLOBALS['client_id']],
		json_encode([
			'filter' => [
				'visibility' => 'ALL'
			], 
			'page' => $page, 
			'page_size' => $page_size
		]),
		'POST'
	);

	$res = json_decode($_list, 1);

	if (!empty($res['result'])) {
		$ids = array_column($res['result']['items'],'offer_id');
		return $ids;
	} else {
		return false;
	}
}

$ids = ozonGetProducts();
$ids = array_merge($ids, ozonGetProducts(2));
$ids = array_merge($ids, ozonGetProducts(3));

foreach ($ids as $id) {

	$send[$id] = [
		"offer_id" => $id,
		"stock" => 0,
	];
}

$chunk = array_chunk($send,99);
$result = [];
foreach ($chunk as $k => $group) {
	$res = nksSend(
		'http://api-seller.ozon.ru/v1/product/import/stocks',
		['Api-Key: '.$GLOBALS['api_key'],'Content-Type: application/json;charset=utf-8', 'Client-Id: '.$GLOBALS['client_id']],
		json_encode(['stocks' => $group]),
		'POST'
	);
	$result = array_merge($result, json_decode($res, 1));	
}
$log = [];
foreach ($result['result'] as $key => $value) {
	echo '<pre>';
	print_r($send[$value['offer_id']]);
	echo '</pre><hr/>';
	echo '<pre>';
	print_r($value);
	echo '</pre>';
	
	if (!empty($value['errors'])) {
		$log[$value['offer_id']] = $value['errors'];
	}
}
file_put_contents(__DIR__.'/stock_log.txt',"================================\r\n" , FILE_APPEND);
file_put_contents(__DIR__.'/stock_log.txt', date('Y-m-d H:i:s')."\r\n" , FILE_APPEND);
file_put_contents(__DIR__.'/stock_log.txt', print_r($log,1)."\r\n" , FILE_APPEND);
