<?
$_SERVER['DOCUMENT_ROOT'] = realpath(__DIR__ . '/../../..');
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];

class Ozon{
	protected $config;
	protected $header;
	protected $hl;
	protected $head;
	protected $footer;
	protected $tpl_order;
	protected $tpl_item;

	function __construct($config)
	{
		$this->config = $config;
		$this->header = ['Client-Id: '.$this->config['client_id'], 'Api-Key: '.$this->config['key'], 'Content-Type: application/json'];
		$this->prepareTemplate();

	}

	function run()
	{
		$this->log('sync_date', 'старт обмена');

		if (file_exists(__DIR__ . '/../files/tmp.xml')) {
			unlink(__DIR__ . '/../files/tmp.xml');
		}
		$orders = $this->getNewOrders();
		#file_put_contents(__DIR__.'/../logs/log.txt', json_encode($orders));
		// $orders = file_get_contents(__DIR__.'/../logs/log.txt');
		// $orders = json_decode($orders, 1);
		$this->startXML();
		$this->processOrders($orders);
		$this->completeXML($orders);
		$last_order_time = file_get_contents(__DIR__.'/../logs/sync.log');
		$this->log('sync_date', 'конец обмена, время последнего заказа: ' . date('Y-m-d H:i:s', $last_order_time));
	}

	function runTest($date_from, $date_to)
	{
		$orders = $this->getOrders($date_from, $date_to);
		return $this->testOrders($orders['result']);
	}

	function testOrders($orders)
	{
		$codes = $this->getCodes();
		$result = [];
		foreach ($orders as $order) {
			$date_insert_mc = strtotime($order['in_process_at']);
			$date_insert = date($this->config['date_format'], $date_insert_mc);
			$shipment_date_mc = strtotime($order['shipment_date']);
			$shipment_date = date($this->config['date_format'], $shipment_date_mc);
			$result['orders'][$order['posting_number']] = [
				'shipment_date' => $shipment_date,
				'date_insert' => $date_insert
			];
			foreach ($order['products'] as $p) {
				if (array_key_exists($p['offer_id'], $codes)) {
					$result['orders'][$order['posting_number']]['items_with_code'][] = [
						'article' => $p['offer_id'],
						'code' => $codes[$p['offer_id']],
						'name' => $p['name'],
						'qty' => $p['quantity'],
						'price' => (int)$p['price'],
					];
				}else{
					$result['orders'][$order['posting_number']]['items_no_code'][] = [
						'article' => $p['offer_id'],
						'err_code' => $codes[$p['offer_id']],
						'name' => $p['name'],
						'qty' => $p['quantity'],
						'price' => (int)$p['price'],
					];
				}
			}
		}
		return $result;
	}

	protected function startXML()
	{
		$this->head = str_replace('#date#', date($this->config['date_format']), $this->head);
		$this->write('tmp.xml', $this->head);
	}

	function processOrders($orders)
	{
		$codes = $this->getCodes();
		$orders_xml = '';
		$last_order_date = '';
		$no_codes = [];
		if (file_exists(__DIR__.'/../files/orders_done.log')) {
			$orders_done = file_get_contents(__DIR__.'/../files/orders_done.log');
			$orders_done = json_decode($orders_done, 1);
		}

		foreach ($orders as $order) {
			if (is_array($orders_done) && in_array($order['posting_number'],$orders_done)) {
				continue;
			}
			$error = false;
			$items = '';
			$date_insert_mc = strtotime($order['in_process_at']);

			$date_insert = date($this->config['date_format'], $date_insert_mc);
			$orders_file_name = date('Y-m-d', $date_insert_mc);

			$shipment_date = strtotime($order['shipment_date']);
			$shipment_date = date($this->config['date_format'], $shipment_date);

			foreach ($order['products'] as $p) {

				if (array_key_exists($p['offer_id'], $codes)) {

					$replace_item = [
						'#code#' => $codes[$p['offer_id']],
						'#name#' => $p['name'],
						'#qty#' => $p['quantity'],
						'#price#' => (int)$p['price'],
					];
					$items .= "\r\n\t\t" . str_replace(array_keys($replace_item), array_values($replace_item), $this->tpl_item);
				}else{
					$no_codes[] = [$order['posting_number'],$date_insert,$p['name'],$p['offer_id']];
					$error = true;
				}
			}
			if (!$error) {
				$replaces = [
					'#id#' => $order['posting_number'],
					'#date_insert#' => $date_insert,
					'#shipment_date#' => $shipment_date,
					'#items#' => $items
				];
				$element = "\r\n\t" . str_replace(array_keys($replaces), array_values($replaces), $this->tpl_order);
				$orders_xml .= $element;
				$this->write(date('Y-m-d').'.xml', $element);
				$orders_done[] = $order['posting_number'];
				$this->write('orders_done.log', json_encode($orders_done), false);
				@file_put_contents(__DIR__ . '/../logs/sync.log', $date_insert_mc);
			}
		}
		$this->csv($no_codes);
		
		$full_orders = '';
		if (file_exists(__DIR__.'/../files/'.date('Y-m-d', strtotime('-6 days')).'.xml')) {
			unlink(__DIR__.'/../files/'.date('Y-m-d', strtotime('-6 days')).'.xml');
		}
		if (file_exists(__DIR__.'/../files/'.date('Y-m-d', strtotime('-5 day')).'.xml')) {
			$full_orders .= file_get_contents(__DIR__.'/../files/'.date('Y-m-d', strtotime('-5 day')).'.xml');
		}
		if (file_exists(__DIR__.'/../files/'.date('Y-m-d', strtotime('-4 day')).'.xml')) {
			$full_orders .= file_get_contents(__DIR__.'/../files/'.date('Y-m-d', strtotime('-4 day')).'.xml');
		}
		if (file_exists(__DIR__.'/../files/'.date('Y-m-d', strtotime('-3 day')).'.xml')) {
			$full_orders .= file_get_contents(__DIR__.'/../files/'.date('Y-m-d', strtotime('-3 day')).'.xml');
		}
		if (file_exists(__DIR__.'/../files/'.date('Y-m-d', strtotime('-2 day')).'.xml')) {
			$full_orders .= file_get_contents(__DIR__.'/../files/'.date('Y-m-d', strtotime('-2 day')).'.xml');
		}
		if (file_exists(__DIR__.'/../files/'.date('Y-m-d', strtotime('-1 day')).'.xml')) {
			$full_orders .= file_get_contents(__DIR__.'/../files/'.date('Y-m-d', strtotime('-1 day')).'.xml');
		}
		$full_orders .= file_get_contents(__DIR__.'/../files/'.date('Y-m-d').'.xml');
		$this->write('tmp.xml', $full_orders);

		// $this->write(date('Y-m-d').'.xml', $orders_xml);
	}

	function getCodes(){

	    $yday = date('Y.m.d', strtotime('-1 day'));
	    if (file_exists(__DIR__.'/../../'.$yday.'.xml')) {
            unlink(__DIR__.'/../../'.$yday.'.xml');
	    }

	    $cday = date('Y.m.d');
	    if (!file_exists(__DIR__.'/../../'.$cday.'.xml')) {
            copy('http://marsgroup.ru/wiki/katalog_nomenklatura.xml',__DIR__.'/../../'.$cday.'.xml');
	    }

	    $cur_list_ids = [];
		$xml = simplexml_load_file(__DIR__.'/../../'.$cday.'.xml');
        foreach ($xml->children() as $child) {
            $cur_list_ids[$child->vendorCode->__toString()] = $child->CodeIM->__toString();
        }

		/*$json = json_encode($xml);
		$loc_array = json_decode($json,TRUE);
		foreach ($loc_array['supplier']['offers']['offer'] as $off) {
			$id = $off['vendorCode'];
			$rep = ["\t"," "," ",chr(9)];
			$cur_list_ids[$id] = $off['CodeIM'];
		}*/

		return $cur_list_ids;
	}

	function csv($lines){
		// $lines = array_merge([['Номер заказа','Дата заказа','Название товара','Артикул']],$lines);
		$fp = fopen(__DIR__ . '/../files/tmp.csv', 'w');
		fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
		foreach ($lines as $fields) {
		    fputcsv($fp, $fields,';');
		}
		fclose($fp);
		@file_put_contents(__DIR__ . '/../files/errorz.csv', file_get_contents(__DIR__ . '/../files/tmp.csv'),FILE_APPEND);
	}

	protected function completeXML()
	{
		$this->write('tmp.xml', $this->footer);
		if (file_exists(__DIR__ . '/../files/' . $this->config['file_name'])) {
			unlink(__DIR__ . '/../files/' . $this->config['file_name']);
		}
		rename(__DIR__ . '/../files/tmp.xml', __DIR__ . '/../files/' . $this->config['file_name']);
		$this->sendFTP();
	}

	function sendFTP(){
		$local = __DIR__ . '/../files/' . $this->config['file_name'];
		$remote = './'.$this->config['file_name'];
		$conn_id = ftp_connect($this->config['ftp_host']);
		$login = ftp_login($conn_id, $this->config['ftp_user'], $this->config['ftp_pass']);
		if (!$login) {
			$this->log('error', 'Ошибка соединения FTP');
		}else{
			if (ftp_put($conn_id, $remote, $local, FTP_ASCII)) {

			} else {
				$this->log('error', 'Не удалось загрузить файл на сервер');
			}
			ftp_close($conn_id);
		}
	}

	function prepareTemplate()
	{
		if (file_exists(__DIR__ . '/../templates/head.xml')) {
			$this->head = file_get_contents(__DIR__ . '/../templates/head.xml');
		} else {
			$this->log('error', 'Не могу найти шаблон');
		}
		if (file_exists(__DIR__ . '/../templates/footer.xml')) {
			$this->footer = file_get_contents(__DIR__ . '/../templates/footer.xml');
		} else {
			$this->log('error', 'Не могу найти шаблон');
		}
		if (file_exists(__DIR__ . '/../templates/order.xml')) {
			$this->tpl_order = file_get_contents(__DIR__ . '/../templates/order.xml');
		} else {
			$this->log('error', 'Не могу найти шаблон заказа');
		}
		if (file_exists(__DIR__ . '/../templates/item.xml')) {
			$this->tpl_item = file_get_contents(__DIR__ . '/../templates/item.xml');
		} else {
			$this->log('error', 'Не могу найти шаблон товара');
		}
	}

	function send($url, $header = [], $params = [], $type = 'GET')
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
		curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		#curl_setopt($ch, CURLOPT_HEADER, 1);
		if (count($header)) {
			curl_setopt ($ch, CURLOPT_HTTPHEADER, $header);
		}

		if ($type == 'GET') {
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_TIMEOUT, 600);
			curl_setopt($ch, CURLOPT_HEADEROPT, CURLHEADER_UNIFIED);
			curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:53.0) Gecko/20100101 Firefox/53.0');
		}
		if ($type == 'POST') {
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		}
		if ($type == 'PUT') {
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
		}
		$result = curl_exec($ch);
		$code = curl_getinfo($ch,CURLINFO_HTTP_CODE);
		curl_close($ch);
		return $result;
	}

	function sendRequest($url, $request, $type){
		return $this->send(
			$this->config['host'].$url,
			$this->header,
			$request,
			$type
		);
	}

	function getOrders($date_from, $date_to, $offset = 0, $page_size = 50)
	{
		$request = [
			"dir" => "asc",
			"filter" => [
				"since" => date(DATE_ATOM, strtotime($date_from)), // date(DATE_ATOM, strtotime('2020-04-13 00:00:00'))
				"to" => date(DATE_ATOM, strtotime($date_to)) // date(DATE_ATOM), // date(DATE_ATOM, strtotime('2020-04-13 12:59:59')),
			],
			"limit" => $page_size,
			"offset" => $offset,
			"with" => [
				"barcodes" => true
			]
		];
		$res = $this->send(
			$this->config['host'].'/v2/posting/fbs/list',
			$this->header,
			json_encode($request),
			'POST'
		);
		return json_decode($res, 1);
	}

	function getNewOrders($offset = 0, $page_size = 50)
	{
		$orders = [];
		/*echo '<pre>';
		print_r(date(DATE_ATOM,$this->config['date_from']));
		echo '</pre><hr/>';*/
		$request = [
			"dir" => "asc",
			"filter" => [
				"since" => date(DATE_ATOM,$this->config['date_from']), // date(DATE_ATOM, strtotime('2020-04-13 00:00:00'))
				"status" => "awaiting_packaging",
				"to" => date(DATE_ATOM) // date(DATE_ATOM), // date(DATE_ATOM, strtotime('2020-04-13 12:59:59')),
			],
			'status' => [
				"awaiting_packaging"
			],
			"limit" => $page_size,
			"offset" => $offset,
			"with" => [
				"barcodes" => true
			]
		];
		$res = $this->send(
			$this->config['host'].'/v2/posting/fbs/list',
			$this->header,
			json_encode($request),
			'POST'
		);
		$res = json_decode($res, 1);

		if (!empty($res['result'])) {
			$orders = $res['result'];
			if (count($res['result']) == $page_size) {
				$offset += $page_size;
				while ($r = $this->getNewOrders($offset)) {

					$orders = array_merge($orders, $r);
				}
			}
			return $orders;
		} else {
			return false;
		}

	}

	function getProd($id)
	{
		$request = [
			"sku" => $id
		];
		$res = $this->send(
			$this->config['host'].'/v2/product/info',
			$this->header,
			json_encode($request),
			'POST'
		);

		$res = json_decode($res, 1);
		if (!empty($res['result']) && !empty($res['result']['barcode'])) {
			return $res['result'];
		} else {
			return false;
		}


	}

	function getProducts(array $ids, $page_num = 1, $page_size = 1000)
	{
		$products = [];
		$result = [];
		$request = [
			"filter" => [
				'product_id' => array_values($ids)
			],
			'page' => $page_num,
			'page_size' => $page_size
		];
		$res = $this->send(
			$this->config['host'].'/v1/product/list',
			$this->header,
			json_encode($request),
			'POST'
		);
		echo '<pre>';
		print_r($res);
		echo '</pre><hr/>';
		exit;
		$res = json_decode($res, 1);
		if (!empty($res['result']['items'])) {
			$products = $res['result']['items'];
			if ($res['result']['total'] > $page_size) {
				$page_num++;
				while ($r = $this->getProducts($ids, $page_num)) {
					$products = array_merge($products, $r);
				}
			}
			foreach ($products['result']['items'] as $item) {
				$result[$item['product_id']] = $item['offer_id'];
			}
			return $result;
		} else {
			return false;
		}

	}

	function write($file, $msg, $file_append = true)
	{
		if ($file_append) {
			@file_put_contents(__DIR__ . '/../files/' . $file, $msg, FILE_APPEND);
		} else {
			@file_put_contents(__DIR__ . '/../files/' . $file, $msg);
		}
	}

	function log($file, $msg, $file_append = true)
	{
		if ($file_append) {
			@file_put_contents(__DIR__ . '/../logs/' . $file . '.log', date('Y-m-d H:i:s') . ' - ' . print_r($msg, 1) . "\r\n", FILE_APPEND);
		} else {
			@file_put_contents(__DIR__ . '/../logs/' . $file . '.log', date('Y-m-d H:i:s') . ' - ' . print_r($msg, 1) . "\r\n");
		}

		if ($file == 'error') {
			exit;
		}
	}
}
