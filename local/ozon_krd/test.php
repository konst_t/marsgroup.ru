<?php
require(__DIR__ . "/config/config.php");
require(__DIR__ . "/classes/source.php");

if (! file_exists(__DIR__ . "/logs")) {
    mkdir(__DIR__ . "/logs");
}

if (! file_exists(__DIR__ . "/files")) {
    mkdir(__DIR__ . "/files");
}

if (file_exists(__DIR__ . "/logs/sync.log")) {
    $config['date_from'] = file_get_contents(__DIR__ . "/logs/sync.log");
}

$ozon = new Ozon($config);
$date = (isset($_GET['date']) && !empty($_GET['date'])) ? $_GET['date'] : date('d.m.Y');
$res = $ozon->runTest($date.' 00:00:00', $date.' 23:59:59');
echo '<pre>';
print_r($res);
echo '</pre><hr/>';