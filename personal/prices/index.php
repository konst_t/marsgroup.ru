<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Персональный раздел");
if (!$USER->IsAuthorized()) {
	LocalRedirect('/auth/');
	exit;
}
?>
<section class="info">
	<div class="info_container container">
		<h1>Прайсы</h1>

		<table class="prices">
			<tr>
				<td>Tutis, коляски, трости, автокресла</td>
				<td><a target="_blank" download href="http://marsgroup.ru/wiki/Price_P1.xlsx">Скачать</a></td>
			</tr>
			<tr>
				<td>Велосипеды, самокаты, транспортная игрушка</td>
				<td><a target="_blank" download href="http://marsgroup.ru/wiki/Price_P3.xlsx">Скачать</a></td>
			</tr>
			<tr>
				<td>Гигиена (пластик), сопутствующие товары</td>
				<td><a target="_blank" download href="http://marsgroup.ru/wiki/Price_P7.xlsx">Скачать</a></td>
			</tr>
			<tr>
				<td>Детский текстиль, выписка</td>
				<td><a target="_blank" download href="http://marsgroup.ru/wiki/Price_P8.xlsx">Скачать</a></td>
			</tr>
			<tr>
				<td>Игрушки, качалки, кукольные коляски</td>
				<td><a target="_blank" download href="http://marsgroup.ru/wiki/Price_P4.xlsx">Скачать</a></td>
			</tr>
			<tr>
				<td>КГТ игровой (горки, домики, песочницы, надувка, качели)</td>
				<td><a target="_blank" download href="http://marsgroup.ru/wiki/Price_P6.xlsx">Скачать</a></td>
			</tr>
			<tr>
				<td>Мебель (кровати, комоды, колыбели), матрасы, аксессуары для пеленания</td>
				<td><a target="_blank" download href="http://marsgroup.ru/wiki/Price_P2.xlsx">Скачать</a></td>
			</tr>
			<tr>
				<td>Наборы мебели, стульчики, манежи, ходунки, прыгунки, шезлонги, электрокачели</td>
				<td><a target="_blank" download href="http://marsgroup.ru/wiki/Price_P5.xlsx">Скачать</a></td>
			</tr>
			<tr>
				<td>Коляски (GESSLEIN)</td>
				<td><a target="_blank" download href="http://marsgroup.ru/wiki/Price_EUR_P9.xlsx">Скачать</a></td>
			</tr>
			<tr>
				<td>Мебель (BREVI, COMBELLE, MIBB, PICCI)</td>
				<td><a target="_blank" download href="http://marsgroup.ru/wiki/Price_EUR_P10.xlsx">Скачать</a></td>
			</tr>
			<tr>
				<td>Текстиль (MIBB, CANDIDE, INTER BABY, NaNan, PICCI, TINEO, TUTTOLINA)</td>
				<td><a target="_blank" download href="http://marsgroup.ru/wiki/Price_EUR_P11.xlsx">Скачать</a></td>
			</tr>
			<tr>
				<td>Зимний ассортимент</td>
				<td><a target="_blank" download href="http://marsgroup.ru/wiki/Price_P9.xlsx">Скачать</a></td>
			</tr>
			<tr>
				<td>Акция (игрушки, коляски)</td>
				<td><a target="_blank" download href="http://marsgroup.ru/wiki/Price_Action_P10.xlsx">Скачать</a></td>
			</tr>
			<tr>
				<td>Акция (текстиль)</td>
				<td><a target="_blank" download href="http://marsgroup.ru/wiki/Price_Action_P11.xlsx">Скачать</a></td>
			</tr>
			<tr>
				<td>ПрайсЛист_УЦЕНКА</td>
				<td><a target="_blank" download href="http://marsgroup.ru/wiki/Price_УЦЕНКА.xlsx">Скачать</a></td>
			</tr>
		</table>



	</div>
</section>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>