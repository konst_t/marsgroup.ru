<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Условия сотрудничества");
?><p>
 <b><span style="font-size: 22pt;">Условия сотрудничества</span></b>
</p>
<p>
	 Наша компания сотрудничает только с юридическими лицами и индивидуальными предпринимателями на основании договора поставки.
</p>
<p>
	 Для заключения договора Вам необходимо предоставить следующие документы.
</p>
<p>
 <b>Для юридических лиц</b>:
</p>
<ul>
	<li>Выписка из ЕГРЮЛ (копия, заверенная организацией),</li>
	<li>Свидетельство о государственной регистрации юридического лица (ОГРН) (копия, заверенная организацией),</li>
	<li>Свидетельство о постановке на налоговый учет (ИНН) (копия, заверенная организацией),</li>
	<li>Устав: 1, 2 страницы с перечнем полномочий генерального директора, последний лист Устава (копия, заверенная организацией),</li>
	<li>Письмо органов статистики (коды статистики) (копия, заверенная организацией),</li>
	<li>Изменения к Уставу при их наличии (копия, заверенная организацией),</li>
	<li>Протокол-решение об избрании руководителя юридического лица (копия, заверенная организацией),</li>
	<li>Приказ о вступлении в должность руководителя юридического лица (копия, заверенная организацией),</li>
	<li>Информационная карточка клиента, содержащая следующие данные: юридический адрес, почтовый адрес, банковские реквизиты, контактные телефоны, в том числе факс, адрес электронной почты, контактное лицо.</li>
</ul>
<p>
 <b>Для индивидуальных предпринимателей:</b>
</p>
<ul>
	<li>Выписка из ЕГРИП (копия, заверенная организацией),</li>
	<li>Свидетельство о государственной регистрации в качестве индивидуального предпринимателя (ОГРН),</li>
	<li>Свидетельство о постановке индивидуального предпринимателя на налоговый учет (ИНН),</li>
	<li>Паспорт (копия),</li>
	<li>Информационная карточка клиента, содержащая следующие данные: юридический адрес, почтовый адрес, банковские реквизиты, контактные телефоны, в том числе факс, адрес электронной почты, контактное лицо.</li>
</ul>
<p>
 <b>Минимальная сумма закупки - от 30 000 рублей. </b>
</p>
 <b> </b>
<p>
 <b> </b>
</p>
 <b> </b>
<p>
 <b>Отпускные (базовые) цены:</b>
</p>
<ul>
	<li>от 30 000 рублей - крупный опт.</li>
</ul>
<p>
 <b>Система скидок:</b>
</p>
<ul>
	<li>от 80 000 - 100 000 рублей - 2%, </li>
	<li>от 300 000 рублей - 3%,</li>
	<li>от 600 000 рублей - 4%,</li>
	<li>при месячном обороте свыше 1 000 000 рублей - 5%.</li>
</ul><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>