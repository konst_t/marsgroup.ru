	<?if($is_info){?>
			 </div>
		</section>
	<?}?>

			<? /*if ($ny_theme) { ?><style>
				.ded{
					position:relative;
					background-image: url("<?=SITE_TEMPLATE_PATH;?>/img/ded.png");
					background-position:bottom center;
					width:100%;
					height:427px;
					z-index:1;
					margin-top:-427px;
				}
				.ded{}
			</style>
			<div class="ded"></div><? }*/ ?>

	<footer>
		<div class="footer_container container">
			<div class="footer_block footer_block_menu">
				<div class="footer_block_header">Компания</div>
				<ul class="footer_block_list">
					<li><a href="/about/contacts/">Контакты</a></li>
					<?/*<li><a href="/about/">О нас</a></li>
					<li><a href="/about/career/">Карьера</a></li>*/?>
				</ul>
			</div>
			<?/*<div class="footer_block footer_block_menu">
				<div class="footer_block_header">Партнерам</div>
				<ul class="footer_block_list">
					<li><a href="/partners/for-providers/">Информация для поставщиков</a></li>
					<li><a href="/partners/conditions/">Условия сотрудничества</a></li>
					<li><a href="/partners/ordering/">Оформление заказа</a></li>
				</ul>
			</div>*/?>
			<div class="footer_block footer_block_menu">
				<div class="footer_block_header">Клиентам</div>
				<ul class="footer_block_list">
					<li><a href="/brends/">Бренды</a></li>
					<? if ($USER->IsAuthorized()){ ?><li><a href="/wiki/Price_all.xls">Скачать прайс</a></li><? } ?>
					<?/*<li><a href="javascript:void(0)">Блог</a></li>*/?>
				</ul>
			</div>
			<div class="footer_block footer_block_fdb">
				<div class="footer_block_header">Подписаться на нашу рассылку</div>
				<div class="footer_fdb">
					<div class="footer_fdb_err"></div>
					<input type="email" placeholder="e-mail" name="mail" required>
					<input type="submit" value="подписаться" class="footer_fdb_subm">
					<span class="footer_fdb_policy">Отправляя данную форму Вы соглашаетесь <br> с условиями <a target="_blank" href="/confidential/">политики конфеденциальности нашего сайта</a>.</span>
				</div>
			</div>
			<div class="footer_block footer_block_soc">
				<div class="footer_block_header">Мы в социальных сетях</div>
				<ul class="footer_soc_list">
					<li class="fb"><a target="_blank" href="https://www.facebook.com/marsgroupru/"></a></li>
					<?//<li class="in"><a target="_blank" href="https://www.instagram.com/marsgroup__/"></a></li>?>
					<li class="in"><a target="_blank" href="https://www.instagram.com/marsgroup.ru/"></a></li>
					<li class="vk"><a target="_blank" href="https://vk.com/marsgroupru"></a></li>
					<li class="ok"><a target="_blank" href="https://ok.ru/profile/574039728274"></a></li>
					<li class="yt"><a target="_blank" href="https://www.youtube.com/channel/UCGgZcqaWkMTV4HESD1n0P9Q?view_as=subscriber"></a></li>
				</ul>
				<?/*<div class="footer_payments"></div>*/?>
			</div>
		</div>
	</footer>

	<div class="bottom_panel">
		<?/*<noindex>Техподдержка сайта: <a href="tel:+74952808212">+7(495)280-82-12 доб.150</a></noindex>*/?>
	</div>

	<div class="pop_wrap">

		<div class="pop_up pop_up_cart">
			<div class="pop_up_close">x</div>
			<div class="pop_up_head">
				Товар добавлен в корзину
			</div>
			<div class="pop_up_buttons">
				<a href="javascript:void(0)" class="pop_up_cont">Продолжить</a>
				<a href="/personal/cart/" class="pop_up_2cart">В корзину</a>
			</div>
		</div>

	</div>

</body>
</html>