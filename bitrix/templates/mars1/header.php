<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use \Bitrix\Main\Loader,
	\Bitrix\Main\Config\Option;

Loader::includeModule('iblock');

Option::set("marsgroup","disable_sales", "N");

/* получаем обои */
$arSelect = Array("ID", "NAME", "DETAIL_PICTURE");
$arFilter = Array("IBLOCK_ID"=>13, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
$arFields = $res->GetNext();
$dackgroundImg = CFile::GetPath($arFields["DETAIL_PICTURE"]);

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8">
	<meta name="yandex-verification" content="bf9d135ea58afdd2" />
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<title><?$APPLICATION->ShowTitle()?></title>

	<?$APPLICATION->ShowHead();?>
	<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH;?>/assets/fancybox/jquery.fancybox.min.css">
	<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH;?>/assets/slick/slick.css">
	<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH;?>/assets/slick/slick-theme.css">
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,700italic,400italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH;?>/css/style.css?<?=time();?>">
	<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH;?>/css/mobile.css?<?=time();?>">
	<style>
		body{
			background-image: url("<?=$dackgroundImg?>");
			background-position:center top;
			background-attachment: fixed;
			background-repeat: no-repeat;
		}
	</style>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<!--script src="<?=SITE_TEMPLATE_PATH;?>/js/jquery-3.2.1.min.js"></script-->
	<script src="<?=SITE_TEMPLATE_PATH;?>/js/jquery-migrate-1.4.1.min.js"></script>
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH;?>/assets/fancybox/jquery.fancybox.min.js"></script>
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH;?>/assets/slick/slick.min.js"></script>
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH;?>/assets/zoom-master/jquery.zoom.min.js"></script>
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH;?>/assets/jquery.maskedinput.min.js"></script>
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH;?>/js/script.js?<?=time();?>"></script>
	<meta name="yandex-verification" content="da6dc7e456c09159" />
	<!-- <script charset="UTF-8" src="//cdn.sendpulse.com/js/push/e16a1372394d6d610537c3580fb39e8e_1.js" async></script> -->
</head>
<body>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(25716686, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true,
        ecommerce:"dataLayer"
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/25716686" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
	<?$APPLICATION->ShowPanel();?>
	<div class="popup_overlay"></div>
	<div class="svg_map"><? include('svg.php');?></div>

<?
$hour = (int)date('G');
$minutes = (int)date('i');
$exchange_duration = 31;
if($hour==6 && $minutes<$exchange_duration) {
?>
<div class="warning_panel">
	<h3>Внимание! На сайте идет обновление базы товаров.</h3>Во время обновления сайт может работать некорректно. Пожалуйста, во избежании потери данных не производите на сайте никаких действий. Вы сможете продолжить работу в течении получаса (приблизительно через <? echo $exchange_duration-$minutes; ?> минут). Приносим свои извенения за неудобства.
	<div>X</div>
</div>
<?
}
?>


	<div class="fon"></div>

	<section class="head">

	<div class="head__container container">
			<div class="head__cart">
				<a href="<?=($USER->IsAuthorized())?'/personal/':'/auth/';?>" class="head__cart_auth head__icon<? if($USER->IsAuthorized()){?> rm35<?}?>">
					<svg height="24" width="24">
						<use xlink:href="#personal"/>
					</svg>
				</a>
				<? if($USER->IsAuthorized()){?>
				<!-- <a href="javascript:void(0)" class="head__cart_fav head__icon">
					<svg  height="24" width="24">
						<use xlink:href="#favorite"/>
					</svg>
				</a> -->
				<?}?>
				<?if($USER->IsAuthorized()){?>
				<a href="/personal/cart/" class="head__cart_cart head__icon">
					<span class="cart_count"><?=cartCount();?></span>
					<svg  height="24" width="24">
						<use xlink:href="#cart"/>
					</svg>
				</a>
			<?/*	<a href="/personal/cart/" class="head__cart_cart_mob mob_el">
					<span class="cart_count"><?=cartCount();?></span>
					<svg  height="29" width="31">
						<use xlink:href="#cart_mob"/>
					</svg>
				</a>  */?>
				<?}?>
			</div>
<?/*			<div class="head__inout mob_el">
				<?if($USER->IsAuthorized()){?>
				<a href="/personal/">Личный кабинет</a>
				<!-- &nbsp;&nbsp;|&nbsp;&nbsp;<a href="javascript:void(0)">Избранное</a> -->
				<?}else{?>
				<a href="/auth/">Вход / Регистрация</a>
				<?}?>
			</div>     */?>
	</div>


	<div class="mobile_panel">

			<a class="head__menu mob_el" href="javascript:void(0)">
				<svg width="21px" height="17px">
					<use xlink:href="#menu-sand"/>
				</svg>
			</a>

	</div>

		<div class="head__container container">
			<div class="top_logo">
				<a href="/">
					<img src="<?=SITE_TEMPLATE_PATH;?>/img/logo.png" class="head__logo">
					<?/*<svg class="head__logo" width="385" height="56">
						<use xlink:href="#main-logo" width="385" height="56"  />
					</svg>
					<svg class="head__logo mob_el head__logo_tab" width="296" height="45">
						<use xlink:href="#logo_tab" width="296" height="45"  />
					</svg>
					<svg class="head__logo mob_el head__logo_mob" width="184" height="27">
						<use xlink:href="#logo_mob" width="184" height="27"  />
					</svg>*/?>
				</a>
			</div>
			<div class="head__phone">
				<?/*<a href="tel:+74952808212"><nobr>+7(495)280 82 12</nobr></a>*/?>
			</div>

	<?$APPLICATION->IncludeComponent(
	"bitrix:search.title",
	"mars",
	array(
		"NUM_CATEGORIES" => "1",
		"TOP_COUNT" => "5",
		"CHECK_DATES" => "Y",
		"SHOW_OTHERS" => "N",
		"PAGE" => SITE_DIR."catalog/",
		"CATEGORY_0_TITLE" => GetMessage("SEARCH_GOODS"),
		"CATEGORY_0" => array(
			0 => "iblock_catalog",
			1 => "iblock_offers",
		),
		"CATEGORY_0_iblock_catalog" => array(
			0 => "2",
		),
		"CATEGORY_OTHERS_TITLE" => GetMessage("SEARCH_OTHER"),
		"SHOW_INPUT" => "Y",
		"INPUT_ID" => "title-search-input",
		"CONTAINER_ID" => "topsearch_container",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"SHOW_PREVIEW" => "Y",
		"PREVIEW_WIDTH" => "75",
		"PREVIEW_HEIGHT" => "75",
		"CONVERT_CURRENCY" => "Y",
		"COMPONENT_TEMPLATE" => "mars",
		"ORDER" => "rank",
		"USE_LANGUAGE_GUESS" => "N",
		"PRICE_VAT_INCLUDE" => "N",
		"PREVIEW_TRUNCATE_LEN" => "",
		"CURRENCY_ID" => "RUB",
		"CATEGORY_0_iblock_offers" => array(
			0 => "3",
		),
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);?>
		</div>
		<div style="clear:both;"></div>
	</section>

	<!-- /.head -->

	<?
	$APPLICATION->IncludeComponent("nks:catalog.menu", "template1", Array(
	"IBLOCK_ID" => "2",
		"BANNERS" => "4",
		"MAX_LEVEL" => "3"
	),
	false
);
	?>


	<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "bc", array(
		"PATH" => "",	// Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
			"SITE_ID" => "s1",	// Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
			"START_FROM" => "0",	// Номер пункта, начиная с которого будет построена навигационная цепочка
			),
		false
		);?>
		<!-- /.bc_container container -->
	</section>
	<!-- /.bc -->

	<?
	$curPage = $APPLICATION->GetCurPage(false);
	$arPages = array('/catalog/','/personal/','/sale/','/about/contacts/','/about/topartners/','/brends/', '/auth/', '/new/');
	$is_info = true;
	foreach ($arPages as $page) {
		if (stristr($curPage,$page)) {
			$is_info = false;
		}
	}
	if ($curPage == '/') {
		$is_info = false;
	}
	if($is_info){?>
		<section class="info">
		<div class="info_container container">
	<?}?>
