<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<section class="menu">
	<div class="menu_container container">
		<div class="mob_el mob_tel"><!--<a href="tel:+74957858816">+7(495)785 88 16</a>-->Закрыть</div>
		<div class="menu_wrap">
			<ul class="menu_lvl_1">
				<?foreach($arResult as $key_1 => $lvl_1) {?>
				<li>
					<a href="<?=(mb_strtolower($lvl_1['NAME']) == 'уценка')?$lvl_1['SECTION_PAGE_URL']:'javascript:void(0)';?>" class="menu_lvl_1_link"><?=$lvl_1['NAME'];?></a>
					<? if(isset($lvl_1['IS_PARENT']) && $lvl_1['IS_PARENT'] == 'Y'){?>
					<ul class="menu_sub menu_sub_col<?=count($lvl_1['SUBSECT'])?>">
						<?foreach($lvl_1['SUBSECT'] as $lvl_2) {?>
						<li>
							<ul class="menu_sub_2">
								<li><a href="<?=$lvl_2['SECTION_PAGE_URL'];?>"><?=$lvl_2['NAME'];?></a></li>
								<? if(is_array($lvl_2['SUBSECT'])){?>
									<?foreach($lvl_2['SUBSECT'] as $lvl_3) {?>
										<li><a href="<?=$lvl_3['SECTION_PAGE_URL'];?>"><?=$lvl_3['NAME'];?></a></li>
									<?}?>
								<?}else{?>
									<li><a href="<?=$lvl_2['SECTION_PAGE_URL'];?>">Все <?=mb_strtolower($lvl_2['NAME']);?></a></li>
								<?}?>
							</ul>
						</li>
						<?}?>
						<? if(isset($lvl_1['BANNER'])){?>
						<li class="menu_banner">
							<a href="<?=$lvl_1['BANNER']['LINK'];?>" class="menu_img" style="background-image: url(<?=$lvl_1['BANNER']['IMG'];?>)"></a>
						</li>
						<?}?>
					</ul>
					<?}?>
				</li>
				<?}?>
				<?//<li><a href="/sale/nam-20-let-/" class="menu_lvl_1_link" style="color:#dc1b85;">Акция 20 лет</a></li>?>
			</ul>
		</div>
	</div>
	<!-- /.menu_container -->
</section>