<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc,
	\Bitrix\Main\Config\Option;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 */

$this->setFrameMode(true);
if (!empty($arResult['NAV_RESULT']))
{
	$navParams =  array(
		'NavPageCount' => $arResult['NAV_RESULT']->NavPageCount,
		'NavPageNomer' => $arResult['NAV_RESULT']->NavPageNomer,
		'NavNum' => $arResult['NAV_RESULT']->NavNum
	);
}
else
{
	$navParams = array(
		'NavPageCount' => 1,
		'NavPageNomer' => 1,
		'NavNum' => $this->randString()
	);
}

//echo '<pre>'; print_r($arResult["ITEMS"]); echo '</pre>';



if ($arParams['PAGE_ELEMENT_COUNT'] > 0 && $navParams['NavPageCount'] > 1)
{
	$showTopPager = $arParams['DISPLAY_TOP_PAGER'];
	$showBottomPager = $arParams['DISPLAY_BOTTOM_PAGER'];
	$showLazyLoad = $arParams['LAZY_LOAD'] === 'Y' && $navParams['NavPageNomer'] != $navParams['NavPageCount'];
}

$templateLibrary = array('popup', 'ajax', 'fx');
$currencyList = '';

if (!empty($arResult['CURRENCIES']))
{
	$templateLibrary[] = 'currency';
	$currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}

$templateData = array(
	'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
	'TEMPLATE_LIBRARY' => $templateLibrary,
	'CURRENCIES' => $currencyList
);
unset($currencyList, $templateLibrary);

$elementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
$elementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
$elementDeleteParams = array('CONFIRM' => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));


$generalParams = array(
	'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
	'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],
	'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
	'RELATIVE_QUANTITY_FACTOR' => $arParams['RELATIVE_QUANTITY_FACTOR'],
	'MESS_SHOW_MAX_QUANTITY' => $arParams['~MESS_SHOW_MAX_QUANTITY'],
	'MESS_RELATIVE_QUANTITY_MANY' => $arParams['~MESS_RELATIVE_QUANTITY_MANY'],
	'MESS_RELATIVE_QUANTITY_FEW' => $arParams['~MESS_RELATIVE_QUANTITY_FEW'],
	'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
	'USE_PRODUCT_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
	'PRODUCT_QUANTITY_VARIABLE' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
	'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
	'ADD_PROPERTIES_TO_BASKET' => $arParams['ADD_PROPERTIES_TO_BASKET'],
	'PRODUCT_PROPS_VARIABLE' => $arParams['PRODUCT_PROPS_VARIABLE'],
	'SHOW_CLOSE_POPUP' => $arParams['SHOW_CLOSE_POPUP'],
	'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
	'COMPARE_PATH' => $arParams['COMPARE_PATH'],
	'COMPARE_NAME' => $arParams['COMPARE_NAME'],
	'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
	'PRODUCT_BLOCKS_ORDER' => $arParams['PRODUCT_BLOCKS_ORDER'],
	'LABEL_POSITION_CLASS' => $labelPositionClass,
	'DISCOUNT_POSITION_CLASS' => $discountPositionClass,
	'SLIDER_INTERVAL' => $arParams['SLIDER_INTERVAL'],
	'SLIDER_PROGRESS' => $arParams['SLIDER_PROGRESS'],
	'~BASKET_URL' => $arParams['~BASKET_URL'],
	'~ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
	'~BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE'],
	'~COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
	'~COMPARE_DELETE_URL_TEMPLATE' => $arResult['~COMPARE_DELETE_URL_TEMPLATE'],
	'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
	'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
	'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
	'BRAND_PROPERTY' => $arParams['BRAND_PROPERTY'],
	'MESS_BTN_BUY' => $arParams['~MESS_BTN_BUY'],
	'MESS_BTN_DETAIL' => $arParams['~MESS_BTN_DETAIL'],
	'MESS_BTN_COMPARE' => $arParams['~MESS_BTN_COMPARE'],
	'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
	'MESS_BTN_ADD_TO_BASKET' => $arParams['~MESS_BTN_ADD_TO_BASKET'],
	'MESS_NOT_AVAILABLE' => $arParams['~MESS_NOT_AVAILABLE']
);

$obName = 'ob'.preg_replace('/[^a-zA-Z0-9_]/', 'x', $this->GetEditAreaId($navParams['NavNum']));




?>


<ul class="product_list clearfix">
	<?
	if (!empty($arResult['ITEMS']))
	{ 
		foreach ($arResult['ITEMS'] as $item)
		{

			$uniqueId = $item['ID'].'_'.md5($this->randString().$component->getAction());
			$areaIds[$item['ID']] = $this->GetEditAreaId($uniqueId);
			$this->AddEditAction($uniqueId, $item['EDIT_LINK'], $elementEdit);
			$this->AddDeleteAction($uniqueId, $item['DELETE_LINK'], $elementDelete, $elementDeleteParams);
			$with_offer = (is_array($item['OFFERS']) && count($item['OFFERS']));
			$f = 0;
			?>
			<li id="<?=$this->GetEditAreaId($uniqueId);?>" class="product_list_item">
				<div class="product_list_item_img" style="background-image: url(<?=$item['PREVIEW_PICTURE']['SRC'];?>)">
					<div class="detail_stickers inlist">
						<div class="detail_sticker ds_new" data-title="Новинка" style="display: <?=($item['PROPERTIES']['NOVINKA']['VALUE'] == "Новинка" ? "block" : "none")?>;"></div>
						<div class="detail_sticker ds_sale" data-title="Скидка" style="display: <?=($item['PROPERTIES']['SKIDKA']['VALUE'] == "Да" ? "block" : "none")?>;"></div>
						<div class="detail_sticker ds_exclusive" data-title="Эксклюзив" style="display: <?=($item['PROPERTIES']['EKSKLYUZIV']['VALUE'] == "Да" ? "block" : "none")?>;"></div>
					</div>
					<a href="<?=$item['DETAIL_PAGE_URL'];?>"></a>
				</div>
				<!-- /.product_list_item_img -->
				<div class="product_list_item_name"><a href="<?=$item['DETAIL_PAGE_URL'];?>"><?=$item['NAME'];?></a></div>
				<!-- /.product_list_item_name -->
				<div class="product_list_item_available"><?=($item['CATALOG_AVAILABLE'] == 'Y')?'<span style="color:#CB0089;">В наличии</span>':'<span style="color:#CCC;">Нет в наличии</span>';?></div>
				<div class="product_list_item_mrc_price">&nbsp;<?if ($item['PRICES']['МРЦ']['DISCOUNT_VALUE']>0) {?><? if($USER->IsAuthorized()){?>РРЦ: <?}?><?=$item['PRICES']['МРЦ']['PRINT_DISCOUNT_VALUE'];?><?}?></div><? if(!$USER->IsAuthorized()){?><br><?}?>
				<? if($USER->IsAuthorized()){?>
					<div class="product_list_item_price">&nbsp;<?=$item['PRICES']['BASE']['PRINT_DISCOUNT_VALUE'];?></div>
					<!-- /.product_list_item_price -->
					<div class="product_list_item_action">
						<?if(Option::get("marsgroup","disable_sales", "N") == "N") {?>
							<?if($with_offer){?>
							<a href="<?=$item['DETAIL_PAGE_URL'];?>" class="product_list_item_2basket"><?=($item['CATALOG_AVAILABLE'] == 'Y')?'Выбрать цвет':'Временно отсутствует';?></a>
							<?}else{?>

								<a data-id="<?=$item['ID'];?>" href="javascript:void(0)" class="product_list_item_2basket <?=($item['CATALOG_AVAILABLE'] == 'Y')?'add2cart':'';?>">
									<?=($item['CATALOG_AVAILABLE'] == 'Y')?'Добавить в корзину':'Временно отсутствует';?>
								</a>
							<?}?>
						<?}else{?>
							<a href="<?=$item['DETAIL_PAGE_URL'];?>" class="product_list_item_2basket">Подробнее</a>
						<?}?>
					</div>	
				<?}else{?>
					<div class="product_list_item_action">
						<a href="<?=$item['DETAIL_PAGE_URL'];?>" class="product_list_item_2basket">Подробнее</a>
					</div>
				<?}?>
				
				<!-- /.product_list_item_action -->
			</li>
		<?} 
	}?>
</ul>

<? echo $arResult["NAV_STRING"]; ?>