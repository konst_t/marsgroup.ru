<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (isset($arParams["TEMPLATE_THEME"]) && !empty($arParams["TEMPLATE_THEME"]))
{
	$arAvailableThemes = array();
	$dir = trim(preg_replace("'[\\\\/]+'", "/", dirname(__FILE__)."/themes/"));
	if (is_dir($dir) && $directory = opendir($dir))
	{
		while (($file = readdir($directory)) !== false)
		{
			if ($file != "." && $file != ".." && is_dir($dir.$file))
				$arAvailableThemes[] = $file;
		}
		closedir($directory);
	}

	if ($arParams["TEMPLATE_THEME"] == "site")
	{
		$solution = COption::GetOptionString("main", "wizard_solution", "", SITE_ID);
		if ($solution == "eshop")
		{
			$templateId = COption::GetOptionString("main", "wizard_template_id", "eshop_bootstrap", SITE_ID);
			$templateId = (preg_match("/^eshop_adapt/", $templateId)) ? "eshop_adapt" : $templateId;
			$theme = COption::GetOptionString("main", "wizard_".$templateId."_theme_id", "blue", SITE_ID);
			$arParams["TEMPLATE_THEME"] = (in_array($theme, $arAvailableThemes)) ? $theme : "blue";
		}
	}
	else
	{
		$arParams["TEMPLATE_THEME"] = (in_array($arParams["TEMPLATE_THEME"], $arAvailableThemes)) ? $arParams["TEMPLATE_THEME"] : "blue";
	}
}
else
{
	$arParams["TEMPLATE_THEME"] = "blue";
}

$arParams["FILTER_VIEW_MODE"] = (isset($arParams["FILTER_VIEW_MODE"]) && toUpper($arParams["FILTER_VIEW_MODE"]) == "HORIZONTAL") ? "HORIZONTAL" : "VERTICAL";
$arParams["POPUP_POSITION"] = (isset($arParams["POPUP_POSITION"]) && in_array($arParams["POPUP_POSITION"], array("left", "right"))) ? $arParams["POPUP_POSITION"] : "left";

$nav = CIBlockSection::GetNavChain(2, $arResult['SECTION']['ID'],array('ID','NAME','SECTION_PAGE_URL', 'DEPTH_LEVEL','IBLOCK_SECTION_ID'));
$fullSect = array();
while($chain = $nav->GetNext()){
	/*if ($z) {
		break;
	}
	$z = 1;
	if ($chain['ID'] == $arResult['SECTION']['ID'] && $chain['DEPTH_LEVEL'] != 1) {
		break;
	}
	if ($chain['DEPTH_LEVEL'] == 1) {*/
	if ($chain['DEPTH_LEVEL'] != 2) {
		continue;
	}
	if ($chain['DEPTH_LEVEL'] == 2) {
		$fullSect['ID'] = $chain['ID'];
		$fullSect['NAME'] = $chain['NAME'];
		$fullSect['URL'] = $chain['SECTION_PAGE_URL'];
	}

	$arrrFilter = array("IBLOCK_ID"=>2,"SECTION_ID"=>$chain['ID'],"ACTIVE"=>"Y");
	$obj = CIBlockSection::GetList(Array("left_margin"=>"asc"), $arrrFilter,false,array('ID','NAME','SECTION_PAGE_URL', 'DEPTH_LEVEL','IBLOCK_SECTION_ID'));
	$arSectTmp = array();
	while($arList = $obj->GetNext())
	{
		$arSectTmp[$arList['ID']]['ID'] = $arList['ID'];
		$arSectTmp[$arList['ID']]['NAME'] = $arList['NAME'];
		$arSectTmp[$arList['ID']]['URL'] = $arList['SECTION_PAGE_URL'];
		if($arList["ID"] == $arResult['SECTION']['ID']){			
			$arSectTmp[$arList['ID']]["CURRENT"]="Y";
		}else{
			$arSectTmp[$arList['ID']]["CURRENT"]="N";
		}
		$arFilter2 = array("IBLOCK_ID"=>2,"SECTION_ID"=>$arList['ID'],"ACTIVE"=>"Y");
		$obj2 = CIBlockSection::GetList(Array("left_margin"=>"asc"), $arFilter2,false,array('ID','NAME','SECTION_PAGE_URL', 'DEPTH_LEVEL','IBLOCK_SECTION_ID'));
		$arSectTmp2 = array();
		while($arList2 = $obj2->GetNext())
		{
			$arSectTmp2[$arList2['ID']]['ID'] = $arList2['ID'];
			$arSectTmp2[$arList2['ID']]['NAME'] = $arList2['NAME'];
			$arSectTmp2[$arList2['ID']]['URL'] = $arList2['SECTION_PAGE_URL'];
			if($arList2["ID"] == $arResult['SECTION']['ID']){			
				$arSectTmp2[$arList2['ID']]["CURRENT"]="Y";
			}else{
				$arSectTmp2[$arList2['ID']]["CURRENT"]="N";
			}
			$arFilter3 = array("IBLOCK_ID"=>2,"SECTION_ID"=>$arList2['ID'],"ACTIVE"=>"Y");
			$obj3 = CIBlockSection::GetList(Array("left_margin"=>"asc"), $arFilter3,false,array('ID','NAME','SECTION_PAGE_URL', 'DEPTH_LEVEL','IBLOCK_SECTION_ID'));
			$arSectTmp3 = array();
			while($arList3 = $obj3->GetNext())
			{
				$arSectTmp3[$arList3['ID']]['ID'] = $arList3['ID'];
				$arSectTmp3[$arList3['ID']]['NAME'] = $arList3['NAME'];
				$arSectTmp3[$arList3['ID']]['URL'] = $arList3['SECTION_PAGE_URL'];
				if($arList3["ID"] == $arResult['SECTION']['ID']){			
					$arSectTmp3[$arList3['ID']]["CURRENT"]="Y";
				}else{
					$arSectTmp3[$arList3['ID']]["CURRENT"]="N";
				}				
			}
			if (count($arSectTmp3)) {
				$arSectTmp2[$arList2['ID']]['IS_PARENT'] = 'Y';
				$arSectTmp2[$arList2['ID']]['SUBCAT'] = $arSectTmp3;
			}else{
				$arSectTmp2[$arList2['ID']]['IS_PARENT'] = 'N';			
			}
			
		}
		$arSectTmp[$arList['ID']]['SUBCAT'] = $arSectTmp2;
		if (count($arSectTmp2)) {
			$arSectTmp[$arList['ID']]['IS_PARENT'] = 'Y';
		}else{
			$arSectTmp[$arList['ID']]['IS_PARENT'] = 'N';			
		}
	}
	$fullSect['SUBCAT'] = $arSectTmp;
	$prevID = $chain['ID'];
}

$arResult['SUBSECT'] = $fullSect;