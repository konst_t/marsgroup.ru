<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<ul class="shops_list">
	<?foreach($arResult['ITEMS'] as $office) {?>
	<li>
		<strong><?=$office['PROPERTIES']['OF_CITY']['VALUE'];?></strong>
		<p><?=$office['PROPERTIES']['OF_ADR']['VALUE'];?></p>
		<p>Телефон: <?=$office['PROPERTIES']['OF_TEL']['VALUE'];?></p>
		<? if(!empty($office['PROPERTIES']['OF_SITE']['VALUE'])){?>
		<p>Сайт: <?=$office['PROPERTIES']['OF_SITE']['VALUE'];?></p>
		<?}?>
		<? if(!empty($office['PROPERTIES']['OF_EMAIL']['VALUE'])){?>
		<p>E-mail: <?=$office['PROPERTIES']['OF_EMAIL']['VALUE'];?></p>
		<?}?>
	</li>
	<?}?>
</ul>