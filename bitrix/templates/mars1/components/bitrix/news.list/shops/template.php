<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<ul class="shops_list">
	<?foreach($arResult['ITEMS'] as $shop) {?>
	<li class="<?=($shop['IBLOCK_SECTION_ID'] == 16104)?'shop_velo':'shop_kid';?>">
		<strong><?=$shop['PROPERTIES']['SHOP_CITY']['VALUE'];?></strong>
		<p><?=$shop['PROPERTIES']['SHOP_ADR']['VALUE'];?></p>
		<p>Телефон: <?=$shop['PROPERTIES']['SHOP_TEL']['VALUE'];?></p>
		<p>Время работы: <?=$shop['PROPERTIES']['SHOP_TIME']['VALUE'];?></p>
		<? if(!empty($shop['PROPERTIES']['SHOP_SITE']['VALUE'])){?>
		<p>Сайт: <?=$shop['PROPERTIES']['SHOP_SITE']['VALUE'];?></p>
		<?}?>
		<? if(!empty($shop['PROPERTIES']['SHOP_EMAIL']['VALUE'])){?>
		<p>Email: <?=$shop['PROPERTIES']['SHOP_EMAIL']['VALUE'];?></p>
		<?}?>
	</li>
	<?}?>
</ul>