<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="main_recom_head"><span>Наши бренды</span></div>
<div class="brands_fixed">
<? 
for ($i = 0; $i < 5; $i++) { 
	$item = $arResult["ITEMS"][$i];
	unset($arResult["ITEMS"][$i]);
	$this->AddEditAction($item['ID'], $item['EDIT_LINK'], CIBlock::GetArrayByID($item["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($item['ID'], $item['DELETE_LINK'], CIBlock::GetArrayByID($item["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
?>
		<div class="brands_fixed_item" id="<?=$this->GetEditAreaId($item['ID']);?>">
			<a href="<?=$item["DETAIL_PAGE_URL"]?>">
				<img src="<?=$item["DETAIL_PICTURE"]["SRC"]?>"><br><br>
				<?//=$item['NAME'];?>
			</a>
		</div>
	<?
}?>
	<div style="clear:both;"></div>
</div>
<div class="brand_slider">
	<?foreach($arResult["ITEMS"] as $item) { ?>
		<?
		$this->AddEditAction($item['ID'], $item['EDIT_LINK'], CIBlock::GetArrayByID($item["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($item['ID'], $item['DELETE_LINK'], CIBlock::GetArrayByID($item["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
		<div class="brand_slider_item" id="<?=$this->GetEditAreaId($item['ID']);?>">
			<a href="<?=$item["DETAIL_PAGE_URL"]?>">
				<img src="<?=$item["DETAIL_PICTURE"]["SRC"]?>"><br><br>
				<?//=$item['NAME'];?>
			</a>
		</div>
	<?}?>
</div>