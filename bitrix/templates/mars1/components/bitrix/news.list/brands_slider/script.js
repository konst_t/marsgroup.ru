$(function(){

	$(document).ready(function() {
		$(".brand_slider").slick({
			dots: !1,
			verticalSwiping: false,
			slidesToShow: 5,
			slidesToScroll: 1,
			infinite: false,
			focusOnSelect: true,
			swipeToSlide: true,
			arrows: true,
			autoplay: true,
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 3
					}
				},
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 3
					}
				},
				{
					breakpoint: 590,
					settings: {
						slidesToShow: 2
					}
				},
				{
					breakpoint: 370,
					settings: {
						slidesToShow: 1
					}
				},
			]
		});
	});

});