<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

foreach($arResult["ITEMS"] as $key => $item) {
	if (!isset($item["DETAIL_PICTURE"]["SRC"])){
		unset($arResult["ITEMS"][$key]);
	}
}