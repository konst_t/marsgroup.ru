<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();
//----nks{ общий массив с картинками }
$arAllPict = array();
if (!is_array($arResult['OFFERS']) || !count($arResult['OFFERS'])) {
	if (isset($arResult['DETAIL_PICTURE']['ID'])) {
		$arAllPict[0][] = setResizeArray($arResult['DETAIL_PICTURE']['ID'],array(110,150),array(442,442),array(1920,1080));
	}
	else {
		$arAllPict[0][] = Array(
			"LG"=>SITE_TEMPLATE_PATH."/img/default-no-image-442.png",
			"MD"=>SITE_TEMPLATE_PATH."/img/default-no-image-442.png",
			"SM"=>SITE_TEMPLATE_PATH."/img/default-no-image-92.png",
			"ns"=>1
		);
	}
	if (!empty($arResult['PROPERTIES']['MORE_PHOTO']['VALUE'])) {
		foreach ($arResult['PROPERTIES']['MORE_PHOTO']['VALUE'] as $id) {
			$arAllPict[0][] = setResizeArray($id,array(110,150),array(442,442),array(1920,1080));
		}
	}
}else{
	foreach ($arResult['OFFERS'] as $key => $item) {
		if (isset($item['DETAIL_PICTURE']['ID'])) {
			$arAllPict[$key][] = setResizeArray($item['DETAIL_PICTURE']['ID'],array(110,150),array(442,442),array(1920,1080));
		}
		else {
			$arAllPict[$key][] = Array(
				"LG"=>SITE_TEMPLATE_PATH."/img/default-no-image-442.png",
				"MD"=>SITE_TEMPLATE_PATH."/img/default-no-image-442.png",
				"SM"=>SITE_TEMPLATE_PATH."/img/default-no-image-92.png",
				"ns"=>1
			);
		}
		if (!empty($item['PROPERTIES']['MORE_PHOTO']['VALUE'])) {
			foreach ($item['PROPERTIES']['MORE_PHOTO']['VALUE'] as $id) {
				$arAllPict[$key][] = setResizeArray($id,array(110,150),array(442,442),array(1920,1080));
			}
		}
}}
$arResult['IMGS'] = $arAllPict;

// echo '<pre>';
// print_r($arResult['IMGS']);
// echo '</pre><hr/>';