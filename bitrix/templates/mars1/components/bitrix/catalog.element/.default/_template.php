<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */
global $USER;
$this->setFrameMode(true);
$mainId = $this->GetEditAreaId($arResult['ID']);
$is_offers = (is_array($arResult['OFFERS']) && count($arResult['OFFERS']));
if ($is_offers) {
	$curEl = $arResult['OFFERS'][$arResult['OFFERS_SELECTED']];
}else{
	$curEl = $arResult;
}
?>

<section class="detail clearfix">
	<div class="container detial_container">
		<section class="detail_imgs clearfix">
			<div class="detail_imgs_list">
				<ul class="detail_imgs_ul">
					<?foreach($arResult['IMGS'] as $id => $imgs) {
						foreach($imgs as $img) {?>
						<li data-group="<?=$id;?>"><img src="<?=$img['SM'];?>" alt="<?=$arResult['OFFERS'][$id]['NAME'];?>"></li>
						<?}
					}?>
				</ul>
			</div>
			<!-- /.detail_imgs_list -->
			<div class="detail_imgs_main">
				<ul class="detail_imgs_main_ul">
					<?foreach($arResult['IMGS'] as $id => $imgs) {
						foreach($imgs as $img) {?>
						<li data-lg="<?=$img['LG'];?>" data-group="<?=$id;?>">
							<div class="detail_fancy" data-fg="detail_fancybox_img" data-fancybox="<?=$id;?>" href="<?=$img['LG'];?>">
								<img style="display: none" src="<?=$img['LG'];?>">
								<img data-lg="<?=$img['LG'];?>" src="<?=$img['MD'];?>" alt="<?=$arResult['OFFERS'][$id]['NAME'];?>" oncontextmenu="return false;">
							</div></li>
						<?}
					}?>
				</ul>
				<div id="zoom_area"></div>
			</div>
			<!-- /.detail_imgs_main -->
		</section>
		<!-- /.detail_imgs -->

		<section class="detail_info">
			<h1 class="detail_h1">
				<?=$curEl['NAME'];?>
			</h1>
			<?if(!$USER->IsAuthorized()){?>
			<div class="detail_authorize_warning">
				Для просмотра цены <a href="/auth/">авторизуйтесь или оставьте заявку на регистрацию</a>.
			</div>
			<?}?>
			<ul class="detail_codes">
				<li>Код: <span class="detail_1c_code"><?=$curEl['PROPERTIES']['KOD']['VALUE'];?></span></li>
				<li>Артикул: <span class="detail_vendor_code"><?=$curEl['PROPERTIES']['ARTNUMBER']['VALUE'];?></span></li>
			</ul>
			<? if(!empty($arResult['PROPERTIES']['OBEM']['VALUE']) || !empty($arResult['PROPERTIES']['VES']['VALUE'])){?>
				<ul class="detail_vol_wieght">
					<? if($arResult['PROPERTIES']['VES']['VALUE']){?>
						<li>Вес, <span class="detail_wieght">кг <?=$arResult['PROPERTIES']['VES']['VALUE'];?></span></li>
					<?}?>
					<? if($arResult['PROPERTIES']['OBEM']['VALUE']){?>
						<li>Объем, <span class="detail_volume">м<sup>3</sup> <?=$arResult['PROPERTIES']['OBEM']['VALUE'];?></span></li>
					<?}?>
				</ul>
			<?}?>

			<?if($USER->IsAuthorized()){?>
				<div class="detail_price"><?=$curEl['PRICES']['BASE']['PRINT_DISCOUNT_VALUE'];?></div>
			<?}?>
			<?if($is_offers){?>
			<div class="detail_sku">
				<div class="detail_sku_head">Выберите цвет:</div>
				<ul class="detail_sku_list">
					<?foreach($arResult['OFFERS'] as $k => $offer) {?>
					<li
						data-group="<?=$k;?>"
						data-id="<?=$offer['ID'];?>"
						data-kod="<?=$offer['PROPERTIES']['KOD']['VALUE'];?>"
						data-artnumber="<?=$offer['PROPERTIES']['ARTNUMBER']['VALUE'];?>"
						data-name="<?=$offer['NAME'];?>"
						data-canbuy="<?=$offer['CAN_BUY'];?>"
						data-curprice="<?=$offer['PRICES']['BASE']['PRINT_DISCOUNT_VALUE'];?>"
						class="<?if(!$ao){echo 'detail_sku_active'; $ao=1;}?>"
						>
						<img src="<?=$arResult['IMGS'][$k][0]['SM'];?>">
					</li>
					<?}?>
				</ul>
			</div>
			<?}?>
			<?if($USER->IsAuthorized()){?>
			<div class="detail_add2cart">
				<input type="number" size="3" name="qty" min="1" value="1">
				<a data-id="<?=$curEl['ID'];?>" href="javascript:void(0)" class="detail_add2cart_button <? if($curEl['CAN_BUY'] == 1){?>add2cart<?}?>"><?=($curEl['CAN_BUY'] == 1)?'Добавить в корзину':'Временно отсутствует'?></a>
			</div>
			<!-- /.detail_add2cart -->
			<?}?>
		</section>
		<!-- /.detail_info -->
	</div>
	<!-- /.container detial_container -->
	<div class="detail_desc_container container">
		<div class="detail_desc">
<?/*			<div class="detail_desc_head">
				<img src="<?=SITE_TEMPLATE_PATH;?>/img/detail_desc.png" alt="">
			</div>                                                                 */?>

			<dl class="detail_dl">
				<dt class="dt_desc active"><div class="dt_inner">Описание</div></dt>
				<dd class="dd_desc active">
					<?=$arResult['DETAIL_TEXT'];?>
				</dd>
				<dt class="dt_specs"><div class="dt_inner">Характеристики</div></dt>
				<dd class="dd_specs">
					<ul>
						<? if(!empty($arResult['PROPERTIES']['GARANTIYNYY_SROK_SLUZHBY']['VALUE'])){?>
						<li><?=$arResult['PROPERTIES']['GARANTIYNYY_SROK_SLUZHBY']['NAME'];?>: <?=$arResult['PROPERTIES']['GARANTIYNYY_SROK_SLUZHBY']['VALUE'];?></li>
						<?}?>
						<? if(!empty($arResult['PROPERTIES']['BREND']['VALUE'])){?>
						<li><?=$arResult['PROPERTIES']['BREND']['NAME'];?>: <?=$arResult['PROPERTIES']['BREND']['VALUE'];?></li>
						<?}?>
						<? if(!empty($arResult['PROPERTIES']['STRANA_PROIZVODITEL']['VALUE'])){?>
						<li><?=$arResult['PROPERTIES']['STRANA_PROIZVODITEL']['NAME'];?>: <?=$arResult['PROPERTIES']['STRANA_PROIZVODITEL']['VALUE'];?></li>
						<?}?>
					</ul>
				</dd>
			</dl>
		</div>
		<!-- /.detail_desc -->
	</div>
	<!-- /.detail_desc_container container -->
</section>