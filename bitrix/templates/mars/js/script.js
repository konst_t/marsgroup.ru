$(function(){
	var w = $(document).width();
	$(window).on('resize',function(e){
		w = $(document).width();
		resizeSibling('.main_video','.main_video_img');
	})
	$('.popup_overlay').on('click',function(e){
		closeAll($(this));
	})

	//----nks{ desktops }
	$('.menu_lvl_1 > li').on('mouseenter',function(){
		if(!checkDesktop(1250)) return;
		if(window.tm !== undefined){
			clearTimeout(tm); 
		}
		var el = $(this);
		el.siblings('.active').removeClass('active').children('.menu_sub').fadeOut(200);
		tm = setTimeout(function(){
			setTimeout(function(){	
				openDesktopMenu(el);
			},200);
		},200);
		return false;
	})
	$('.menu_wrap').on('mouseleave',function(){
		if(!checkDesktop(1250)) return;
		closeDesktopMenu();
		return false;
	})
	//----nks{ mobile }
	$('.menu_lvl_1_link').on('click',function(e){
		if(checkDesktop(1250)) return;
		var el = $(this);
		
		el.parent().siblings().children('a.open').removeClass('open').siblings('.menu_sub').slideUp().children().each(function(i){
			$(this).addClass('1111');	
			$(this).find('.menu_sub_2 li').each(function(cnt){
				$(this).addClass('2222');				
				if(cnt == 0){
					$(this).removeClass('open');
				}else{
					$(this).slideUp();
				}
			})
		});
		el.toggleClass('open').siblings('.menu_sub').each(function(i){
			var cnt = i;
			$(this).slideToggle(300,function(){
				if(cnt == 0){	
					setTimeout(function(){
						moveToOpen(el,100,-50);
					},100);
				}
			});
		})
	})	
	$('.menu_sub_2 > li:first-child').on('click',function(e){
		if(checkDesktop(1250)) return;
		e.preventDefault();
		var el = $(this);
		el.parent().parent().siblings().each(function(){
			$(this).children().children().eq(0).removeClass('open').siblings().slideUp();
		})
		
		el.toggleClass('open').siblings().each(function(i){
			var cnt = i;
			$(this).slideToggle(300,function(){
				if(cnt == 0){					
					setTimeout(function(){
						moveToOpen(el,100,-50);
					},100);	
				}
			});
		})
	})

	$('.head__menu').on('click',function(e){
		$('.popup_overlay').fadeIn(300);
		$('.menu').addClass('open');
	})
	$('.menu').on('click',function(e){
		e.stopPropagation();
	})
	$('.filter_block_head').on('click',function(e){
		var el = $(this);
		el.siblings('.filter_block_body').slideToggle(400,function(){
			el.toggleClass('open');

		})
	})
	//----nks{ раскривушка для фильтра }
	$('.menu_current').parent('.cat_left_submenu').parent().addClass('open_sub').parent('.cat_left_submenu').parent('li').addClass('open_sub');
	$('.cat_left_icon').on('click',function(e){
		var el = $(this).parent();
		$(this).siblings('.cat_left_submenu').slideToggle(300,function(){
			el.toggleClass('open_sub');
		})
	})

	$('.catalog_sort_select').on('change',function(e){
		$(this).closest('form').submit();
	})
	$('.footer_block_menu .footer_block_header').on('click',function(e){
		if(checkDesktop(1250)) return;
		$(this).toggleClass('open').siblings().slideToggle();
	})
	$('.mob_catalog_show_filter').on('click',function(e){
		$('.popup_overlay').fadeIn();
		$('.catalog_sect_filter').addClass('open');
	})
	if(w <= 1024){
		$('.filter_block_head ').removeClass('open');
	}

	//----nks{ боковой слайдер }
	var d_list = $('.detail_imgs_ul'),
	d_list_li = $('li',d_list);
	var d_list_s = d_list.slick({
		dots: !1,
		vertical: true,
		verticalSwiping: false,
		slidesToShow: 4,
		slidesToScroll: 4,
		infinite: false,
		focusOnSelect: true,
		swipeToSlide: true
	});
	d_list.on('reInit', function () {
		d_list_li = $('li',d_list);
	})

	//----nks{ основной слайдер }
	var d_main = $('.detail_imgs_main_ul'),
	d_main_li = $('li',d_main);
	var d_main_s = $('.detail_imgs_main_ul').slick({
		slidesToShow: 1,
		arrows: false,
		infinite: false,
		responsive: [
			{
				breakpoint: 1250,
				settings: {
					dots: true
				}
			},
		]
	});
	d_main.on('reInit', function () {
		d_main_li = $('li',d_main);
	})

	//----nks{ связываем слайдеры }
	d_list.find('li').on('click',function(e){
		var el = $(this);
		var ind = el.index();
		el.addClass('nks-slick-current').siblings().removeClass('nks-slick-current');
		d_list_s.slick('slickGoTo', ind, !1);
		d_main_s.slick('slickGoTo', ind, !1);
	})
	d_main.on('afterChange', function (s,sl) {
		d_list_s.slick('slickGoTo', sl.currentSlide, !1);
		d_list.find('li').eq(sl.currentSlide).trigger('click');
    })

    //----nks{ слайдер SKU }
    $('.detail_sku_list').slick({
		slidesToShow: 4,
		slidesToScroll: 4,
		infinite: false,
		focusOnSelect: false,
		swipeToSlide: false,
		responsive: [
			{
				breakpoint: 470,
				settings: {
					arrows: false,
					dots: true
				}
			},
		]
	});
    //----nks{ зум }
	if(checkDesktop(1250)){	
		
		var zoom = $('.detail_imgs_main_ul li > div').zoom({
			target: '#zoom_area'
		});		
	}

	//----nks{ фэнсибокс }
	var fb = $().fancybox({
		selector : '[data-fg="detail_fancybox_img"]',
		loop     : false,
		afterShow: function( instance, current ) {
			d_list_s.slick('slickGoTo', instance.currIndex, !1);
			d_main_s.slick('slickGoTo', instance.currIndex, !1);
			d_list.find('li').eq(instance.currIndex).trigger('click');
		},
		afterClose: function( instance, current ) {
			d_list_s.slick('slickGoTo', instance.currIndex, !1);
			d_main_s.slick('slickGoTo', instance.currIndex, !1);
			d_list.find('li').eq(instance.currIndex).trigger('click');
		}
	});
	/*d_main_s.on('afterChange', function(ev, slick, currentSlide, nextSlide){
		console.log(currentSlide);
	});*/
	//----nks{ фильтр для торговых предложений }
	$('.detail_sku_list li').on('click',function(e){
		var el = $(this);
		var group = el.data('group'),
	    	id = el.data('id'),
	    	kod = el.data('kod'),
	    	artnumber = el.data('artnumber'),
	    	name = el.data('name'),
	    	canbuy = el.data('canbuy'),
	    	curprice = el.data('curprice');	

		el.addClass('detail_sku_active').siblings().removeClass('detail_sku_active');
		d_main_s.slick('slickUnfilter').slick('slickFilter','[data-group='+group+']');
		d_list_s.slick('slickUnfilter').slick('slickFilter','[data-group='+group+']');
		d_list_s.slick('slickGoTo', 0, 1);
		d_list.find('li').eq(0).trigger('click');
		$('.detail_1c_code').html(kod);
		$('.detail_vendor_code').html(artnumber);
		if(canbuy == '1'){
			$('.detail_add2cart_button').addClass('add2cart').attr('data-id',id).html('Добавить в корзину');
		}else{
			$('.detail_add2cart_button').removeClass('add2cart').attr('data-id',id).html('Временно отсутствует');
		}
		$('.detail_h1').html(name);
		$('.detail_price').html(curprice);

	})
	//d_main_s.slick('slickUnfilter').slick('slickFilter','[data-group=2]');
	//d_list_s.slick('slickUnfilter').slick('slickFilter','[data-group=2]');

	//----nks{ делаем первый слайд активным - именно после фильтрации!!! }
	d_list.find('li').eq(0).addClass('nks-slick-current');
	$('.detail_sku_list li').eq(0).trigger('click');

	if(!checkDesktop(767)){
		$('.detail_dl dt').removeClass('active').siblings().removeClass('active');
	}

	//----nks{ детальные табы }
	$('.detail_dl dt').on('click',function(e){
		if(checkDesktop(767)){
			$(this).addClass('active').siblings().removeClass('active');
			$(this).next('dd').addClass('active');
		}else{
			$(this).toggleClass('active').next('dd').toggleClass('active');
			//$(this).next('dd').addClass('active');
		}
	})
	//----nks{ рекомендуемые }
	$('.detail_recom_list').slick({
		slidesToShow: 5,
		infinite: false,
		focusOnSelect: false,
		swipeToSlide: false,
		slidesToScroll: 5,
		arrows: false,
		dots: true,
		responsive: [
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3
				}
			},
			{
				breakpoint: 680,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 560,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
		]
	});

	$('.contacts_block_head').on('click',function(e){
		$(this).toggleClass('active').next('.contacts_block_list').slideToggle();
	})
	$('.shops_head').on('click',function(e){
		$(this).toggleClass('active').siblings('.shops_head').removeClass('active');
	})

	$('.auth_select li').on('click',function(e){
		$(this).addClass('active').siblings().removeClass('active');
		var ind = $(this).index();
		$('.auth_tab').eq(ind).addClass('active').siblings().removeClass('active');
	})

	//----nks{ верхний слайдер на главной }
    $('.main_top_slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		infinite: true,
		autoplay: true,
		autoplaySpeed: 8000,
		arrows: false,
		dots: false
	});

	//----nks{ слайдер товаров на главной }
    $('.main_recom_list').slick({
		slidesToShow: 5,
		slidesToScroll: 3,
		infinite: true,
		autoplay: true,
		autoplaySpeed: 2000,
		arrows: true,
		prevArrow: '.main_recom_arr.prev',
		nextArrow: '.main_recom_arr.next',
		responsive: [
			{
				breakpoint: 1250,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 4
				}
			},
			{
				breakpoint: 1020,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3
				}
			},
			{
				breakpoint: 790,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
					arrows:false,
					dots: true
				}
			},
			{
				breakpoint: 510,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					arrows:false,
					dots: true
				}
			},
		]
	});
	
	//----nks{ поиск }
	$('.topsearch_send').on('click',function(e){
		$(this).closest('form').submit();
	})

    //----nks{ кидаем в корзинку }
    $('body').on('click','.add2cart',function(e){
		e.preventDefault();
		var id = $(this).attr('data-id');
		var qty = $(this).siblings('input[name=qty]').val();
		popOpen('.pop_up_cart');
		$.post('/bitrix/ajax/ajax.php','add2cart=1&id='+id+'&qty='+qty,function(data){
			$('.cart_count').html(data);
			BX.onCustomEvent('OnBasketChange');
		})
	})
	$('.pop_up_close, .pop_up_cont').on('click',function(e){
		popClose();
	})

	$('.go_filter').on('click',function(e){
		$(this).closest('form').submit();
	})

	$('.auth_butt:not(.reg_butt)').on('click',function(e){
		$(this).closest('form').submit();
	})

	//----nks{ регистрация }
	$('.reg_butt').on('click',function(e){
		var form = $(this).closest('form');
		var vid = form.find('input[name="vid[]"]:checked').length;
		if(vid == 0){
			$('.vid_label').css('color','#CB0089');
		}else{
			$('.vid_label').css('color','');
		}
		if(formVal(form) && vid > 0){
			$.post('/bitrix/ajax/register.php',form.serialize(),function(data){
				var data2 = $.trim(data);
				if(data2 == 'success'){
					form.slideUp().html('').after("<h3 class='reg_thx'>Спасибо за регистрацию. <br> После проверки данных мы с Вами свяжемся</h3>");
				}else{
					var tp = $('.test_res').offset().top;
					$('.test_res').html(data);		
					$('body,html').animate({scrollTop:tp+'px'},300);
				}
			})
		}
	})

	$('input[type=tel]').mask('+7(999)999-99-99');

	//----nks{ подписка в футере }
	$('.footer_fdb_subm').on('click',function(e){
		var mail = $(this).siblings('input[type=email]');
		if(mail.val() == ''){
			mail.addClass('in_err');
		}else{
			mail.removeClass('in_err');
			$.post('/bitrix/ajax/subscribe.php','mail='+mail.val(),function(data){
				var data2 = $.trim(data);
				console.log(data2);
				if(data2 == 'success'){
					$('.footer_fdb').html("Спасибо за подписку. <br> На почту выслано письмо с подтверждением подписки");
				}else{
					$('.footer_fdb_err').html(data);
				}
			})
		}
	})

})
$(window).load(function(){
	/*setTimeout(function(){*/
		resizeSibling('.main_video','.main_video_img');
	/*	},300);*/
})

function closeAll(over){
	over.fadeOut(100);
	$('.menu.open').removeClass('open');
	$('.catalog_sect_filter').removeClass('open');
}

function moveToOpen(el,top,pos){
	totop = top || 50;
	topos = pos || 130;
	if((el.position().top+topos) < $('.menu').scrollTop()){
		return $('.menu').animate({scrollTop:(el.position().top+totop)},300);
	}
}
function checkDesktop(width){
	if($(document).width() > width){
		return true;
	}
	return false;	
}
function openDesktopMenu(el){
	$('.menu_wrap').css('height','562px');
	$('.popup_overlay').fadeIn(200);
	el.children('.menu_sub').slideDown(200);
	el.addClass('active');
}
function closeDesktopMenu(el){
	if(window.tm !== undefined){
		clearTimeout(tm); 
	}
	$('.menu_sub').fadeOut(200);
	$('.menu_wrap').css('height','43px');
	$('.menu_lvl_1 > li').removeClass('active');
	$('.popup_overlay').fadeOut(100);
}
function resizeSibling(target,source){
	if ($(document).width() <= 670) return;
	var h = $(source).height();
	$(target).height(h);
}
function popOpen(el){
	$('.pop_wrap').fadeIn().children(el).show().siblings().hide();
}

function popClose(){
	$('.pop_wrap').fadeOut().children(el).hide();
}

function formVal(form){
	var valid = true;
	form.find(' :input[required]').each(function(i){
		if($(this).val() == ''){
			$(this).addClass('in_err').attr('placeholder','Заполните это поле');
			valid = false;
		}else{
			if($(this).attr('name') == 'pass'){
				var v = $(this).val();
				if(v.length < 6){
					$(this).addClass('in_err').val('').attr('placeholder','Пароль должен быть не меньше 6 символов');
				}else{
					$(this).removeClass('in_err').attr('placeholder','');
				}
			}else{
				$(this).removeClass('in_err').attr('placeholder','');
			}
		}
	})
	return valid;
}