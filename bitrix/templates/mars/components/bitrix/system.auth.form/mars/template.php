<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

CJSCore::Init();
?>

<div class="auth_tab_l clearfix">
<?
if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR'])
	ShowMessage($arResult['ERROR_MESSAGE']);
?>

<?if($arResult["FORM_TYPE"] == "login"):?>

<form name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
<?if($arResult["BACKURL"] <> ''):?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
<?endif?>
<?foreach ($arResult["POST"] as $key => $value):?>
	<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
<?endforeach?>
	<input type="hidden" name="AUTH_FORM" value="Y" />
	<input type="hidden" name="TYPE" value="AUTH" />

	<h3 class="auth_mob_head">Вход</h3>
	<label><?=GetMessage("AUTH_LOGIN")?> *</label>
	<input type="text" name="USER_LOGIN" maxlength="50" value="" size="17" />
	<script>
	BX.ready(function() {
		var loginCookie = BX.getCookie("<?=CUtil::JSEscape($arResult["~LOGIN_COOKIE_NAME"])?>");
		if (loginCookie)
		{
			var form = document.forms["system_auth_form<?=$arResult["RND"]?>"];
			var loginInput = form.elements["USER_LOGIN"];
			loginInput.value = loginCookie;
		}
	});
	</script>
	<label><?=GetMessage("AUTH_PASSWORD")?> *</label>
	<input type="password" name="USER_PASSWORD" maxlength="50" size="17" autocomplete="off" />

	<div class="auth_tab_checks">
		<input type="checkbox" id="USER_REMEMBER_frm" name="USER_REMEMBER" value="Y" />
		<label for="USER_REMEMBER_frm">Запомнить меня</label>
		<a href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" class="forgot">Забыли свой пароль?</a>
	</div>
	<a class="auth_butt" href="javascript:void(0)">Вход</a>
	<!--input class="auth_butt" type="submit" name="Login" value="<?=GetMessage("AUTH_LOGIN_BUTTON")?>" /-->
</form>

<?
else:
?>

<form action="<?=$arResult["AUTH_URL"]?>">
	<table width="95%">
		<tr>
			<td align="center">
				<?=$arResult["USER_NAME"]?><br />
				[<?=$arResult["USER_LOGIN"]?>]<br />
				<a href="<?=$arResult["PROFILE_URL"]?>" title="<?=GetMessage("AUTH_PROFILE")?>"><?=GetMessage("AUTH_PROFILE")?></a><br />
			</td>
		</tr>
		<tr>
			<td align="center">
			<?foreach ($arResult["GET"] as $key => $value):?>
				<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
			<?endforeach?>
			<input type="hidden" name="logout" value="yes" />
			<input type="submit" name="logout_butt" value="<?=GetMessage("AUTH_LOGOUT_BUTTON")?>" />
			</td>
		</tr>
	</table>
</form>
<?endif?>
</div>
