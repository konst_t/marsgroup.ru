<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

foreach ($arResult['ITEMS'] as $key => $item) {
	$with_offer = (is_array($item['OFFERS']) && count($item['OFFERS']));
	if ($with_offer) {
		$avail = 'N';
		foreach ($item['OFFERS'] as $key_off => $offer) {
			if ($offer['CATALOG_AVAILABLE'] == 'Y') {
				$avail = 'Y';
			}
		}
		$arResult['ITEMS'][$key]['CATALOG_AVAILABLE'] = $avail;
	}
}