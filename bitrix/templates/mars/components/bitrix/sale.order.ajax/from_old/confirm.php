<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (!empty($arResult["ORDER"]))
{
	?>
	<b><?=GetMessage("SOA_TEMPL_ORDER_COMPLETE")?></b><br /><br />
	<table class="sale_order_full_table">
		<tr>
			<td>
				<?= GetMessage("SOA_TEMPL_ORDER_SUC", Array("#ORDER_DATE#" => $arResult["ORDER"]["DATE_INSERT"], "#ORDER_ID#" => $arResult["ORDER"]["ACCOUNT_NUMBER"]))?>
				<br /><br />
				<?= GetMessage("SOA_TEMPL_ORDER_SUC1", Array("#LINK#" => $arParams["PATH_TO_PERSONAL"])) ?>
			</td>
		</tr>
	</table>
	<?
	if (!empty($arResult["PAY_SYSTEM"]))
	{
		?>
		<br /><br />

		<table class="sale_order_full_table">
			<tr>
				<td class="ps_logo">
					<div class="pay_name"><?=GetMessage("SOA_TEMPL_PAY")?></div>
					<?=CFile::ShowImage($arResult["PAY_SYSTEM"]["LOGOTIP"], 100, 100, "border=0", "", false);?>
					<div class="paysystem_name"><?= $arResult["PAY_SYSTEM"]["NAME"] ?></div><br>
				</td>
			</tr>
			<?
			if (strlen($arResult["PAY_SYSTEM"]["ACTION_FILE"]) > 0)
			{
				?>
				<tr>
					<td>
						<?
						$service = \Bitrix\Sale\PaySystem\Manager::getObjectById($arResult["ORDER"]['PAY_SYSTEM_ID']);

						if ($arResult["PAY_SYSTEM"]["NEW_WINDOW"] == "Y")
						{
							?>
							<script language="JavaScript">
								window.open('<?=$arParams["PATH_TO_PAYMENT"]?>?ORDER_ID=<?=urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))?>&PAYMENT_ID=<?=$arResult['ORDER']["PAYMENT_ID"]?>');
							</script>
							<?= GetMessage("SOA_TEMPL_PAY_LINK", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))."&PAYMENT_ID=".$arResult['ORDER']["PAYMENT_ID"]))?>
							<?
							if (CSalePdf::isPdfAvailable() && $service->isAffordPdf())
							{
								?><br />
								<?= GetMessage("SOA_TEMPL_PAY_PDF", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))."&PAYMENT_ID=".$arResult['ORDER']["PAYMENT_ID"]."&pdf=1&DOWNLOAD=Y")) ?>
								<?
							}
						}
						else
						{
							if ($service)
							{
								/** @var \Bitrix\Sale\Order $order */
								$order = \Bitrix\Sale\Order::load($arResult["ORDER_ID"]);

								/** @var \Bitrix\Sale\PaymentCollection $paymentCollection */
								$paymentCollection = $order->getPaymentCollection();

								/** @var \Bitrix\Sale\Payment $payment */
								foreach ($paymentCollection as $payment)
								{
									if (!$payment->isInner())
									{
										$context = \Bitrix\Main\Application::getInstance()->getContext();
										$service->initiatePay($payment, $context->getRequest());
										break;
									}
								}
							}
							else
							{
								echo '<span style="color:red;">'.GetMessage("SOA_TEMPL_ORDER_PS_ERROR").'</span>';
							}
						}
						?>
					</td>
				</tr>
				<?
			}
			?>
		</table>
		<?
	}
}
else
{
	?>
	<b><?=GetMessage("SOA_TEMPL_ERROR_ORDER")?></b><br /><br />

	<table class="sale_order_full_table">
		<tr>
			<td>
				<?=GetMessage("SOA_TEMPL_ERROR_ORDER_LOST", Array("#ORDER_ID#" => $arResult["ACCOUNT_NUMBER"]))?>
				<?=GetMessage("SOA_TEMPL_ERROR_ORDER_LOST1")?>
			</td>
		</tr>
	</table>
	<?
}
/*
echo '<pre>';
print_r($arResult["ORDER"]);
echo '</pre>';
*/

?>
<?
/*if (!isset($_GET['nosend'])) {

	$ORDER_ID = CSaleOrder::DoSaveOrder($arBasketItems, $arAdditionalFields, 0, $arErrors);

	if ($ORDER_ID > 0 && empty($arErrors))
	{
		CSaleOrderUserProps::DoSaveUserProfile($USER->GetID(), $arUserResult["PROFILE_ID"], $arUserResult["PROFILE_NAME"], $arUserResult["PERSON_TYPE_ID"], $arUserResult["ORDER_PROP"], $arResult["ERROR"]);
	}

	// mail message
	if (empty($arResult["ERROR"]))
	{
		$strOrderList = "";
		$arBasketList = array();
		$dbBasketItems = CSaleBasket::GetList(
				array("ID" => "ASC"),
				array("ORDER_ID" => $arResult["ORDER_ID"]),
				false,
				false,
				array()
			);
		while ($arItem = $dbBasketItems->Fetch())
		{
			if (CSaleBasketHelper::isSetItem($arItem))
				continue;

			$arBasketList[] = $arItem;
		}

		$arBasketList = getMeasures($arBasketList);

		if (!empty($arBasketList) && is_array($arBasketList))
		{
			
			$strOrderList .= '<table cellspacing="2" cellpadding="2" border="1">';
			$strOrderList .= '<tr>
			<td>Код</td>
			<td>Название</td>
			<td>Количество</td>						
			<td>Цена</td>
			</tr>';
			foreach ($arBasketList as $arItem)
			{	
				$code_tov = '';
				$db_props =  CIBlockElement::GetProperty (25, $arItem['PRODUCT_ID'], array("sort" => "asc"),  array("CODE"=>"KOD_ATTR_S"));
				while($ar_props = $db_props->Fetch()){
					$code_tov = $ar_props["VALUE"];  
				}

				$measureText = (isset($arItem["MEASURE_TEXT"]) && strlen($arItem["MEASURE_TEXT"])) ? $arItem["MEASURE_TEXT"] : GetMessage("SOA_SHT");
				$strOrderList .='<tr>';
				$strOrderList .= '<td>'.$code_tov.'</td>';
				$strOrderList .= '<td><a href="http://www.marsgroup.ru'.$arItem["DETAIL_PAGE_URL"].'">'.htmlspecialchars($arItem["NAME"]).'</a></td>';
				$strOrderList .= '<td>'.htmlspecialchars($arItem["QUANTITY"]).'</td>';							
				$strOrderList .= '<td>'.SaleFormatCurrency($arItem["PRICE"], $arItem["CURRENCY"]).'</td>';
				$strOrderList .= "</tr>";

			}
			$strOrderList .= '</table>';

		}

		$arFields = array(
			"ORDER_ID" => $arResult["ORDER"]["ID"],
			"ORDER_DATE" => Date($DB->DateFormatToPHP(CLang::GetDateFormat("SHORT", SITE_ID))),
			"ORDER_USER" => ( (strlen($arUserResult["PAYER_NAME"]) > 0) ? $arUserResult["PAYER_NAME"] : $USER->GetFormattedName(false)),
			"PRICE" => SaleFormatCurrency($orderTotalSum, $arResult["BASE_LANG_CURRENCY"]),
			"BCC" => COption::GetOptionString("sale", "order_email", "order@".$SERVER_NAME),
			"EMAIL" => (strlen($arUserResult["USER_EMAIL"])>0 ? $arUserResult["USER_EMAIL"] : $USER->GetEmail()),
			"ORDER_LIST" => $strOrderList,
			"SALE_EMAIL" => COption::GetOptionString("sale", "order_email", "order@".$SERVER_NAME),
			"DELIVERY_PRICE" => $arResult["DELIVERY_PRICE"],
			"USER_DESCRIPTION" => $arUserResult["ORDER_DESCRIPTION"],
			"PARTNER" => $partner
		);

		$eventName = "SALE_NEW_ORDER";

		$bSend = true;
		foreach(GetModuleEvents("sale", "OnOrderNewSendEmail", true) as $arEvent)
			if (ExecuteModuleEventEx($arEvent, Array($arResult["ORDER_ID"], &$eventName, &$arFields))===false)
				$bSend = false;

		if($bSend)
		{
			$event = new CEvent;
			$event->Send($eventName, SITE_ID, $arFields, "N");
			header('Location: http://marsgroup.ru'.$_SERVER['REQUEST_URI'].'&nosend=1');
			exit;
		}

		CSaleMobileOrderPush::send("ORDER_CREATED", array("ORDER_ID" => $arFields["ORDER_ID"]));
	}
}*/
?>

