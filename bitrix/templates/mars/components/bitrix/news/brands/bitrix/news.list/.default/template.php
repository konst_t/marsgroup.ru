<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<ul class="brands_list clearfix">

<?foreach($arResult["ITEMS"] as $arItem){?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<li id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="brands_list_item">
		<div class="brands_list_item_img" style="background-image: url(<?=(!empty($arItem['PREVIEW_PICTURE']['SRC']))?$arItem['PREVIEW_PICTURE']['SRC']:'/bitrix/templates/mars/components/bitrix/catalog.section/.default/images/no_photo.png';?>)"><a href="<?=$arItem['DETAIL_PAGE_URL'];?>"></a></div>
		<!-- /.brands_list_item_img -->
		<div class="brands_list_item_name"><a href="<?=$arItem['DETAIL_PAGE_URL'];?>"><?=$arItem['NAME'];?></a></div>
		<!-- /.brands_list_item_name -->
	</li>
<?}?>
<?=$arResult["NAV_STRING"]?>

</ul>
