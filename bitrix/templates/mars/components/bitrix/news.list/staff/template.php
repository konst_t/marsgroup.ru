<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="contacts_block">
	<?foreach($arResult as $sect) {?>
	    <div class="contacts_block_head"><?=$sect['NAME'];?></div>
	    <? if(is_array($sect['ITEMS']) && count($sect['ITEMS'])){?>
			<ul class="contacts_block_list">
		    <?foreach($sect['ITEMS'] as $item) {?>
		        <li>
		        	<strong><?=$item['NAME'];?></strong>, <?=$item['PROPERTIES']['MNG_POSITION']['VALUE'];?>
		        	<? if(!empty($item['PREVIEW_TEXT'])){?>		        	
		        	<p><strong>Регионы:</strong> <?=$item['PREVIEW_TEXT'];?></p>
		        	<?}?>
		        	<p>Контакты: <?=$item['PROPERTIES']['MNG_CONTACTS']['VALUE'];?></p>
		        </li>
		    <?}?>
			</ul>
	    <?}?>
	<?}?>
</div>