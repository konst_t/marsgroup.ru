<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$sections = array();
$_sect = CIBlockSection::GetList(array('SORT'=>'ASC'),array('IBLOCK_ID'=>$arParams['IBLOCK_ID'],'ACTIVE'=>'Y'),false,array('NAME','ID'));
while ($sect = $_sect->GetNext()) {
	$sections[$sect['ID']]['NAME'] = $sect['NAME'];
}
foreach ($arResult['ITEMS'] as $key => $item) {
	$sections[$item['IBLOCK_SECTION_ID']]['ITEMS'][] = $item;
}
$arResult = $sections;