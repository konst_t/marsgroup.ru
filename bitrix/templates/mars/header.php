<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8">
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<title><?$APPLICATION->ShowTitle()?></title>
	<?$APPLICATION->ShowHead();?>
	<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH;?>/css/style.css?<?=time();?>">
	<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH;?>/css/mobile.css?<?=time();?>">
	<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH;?>/assets/fancybox/jquery.fancybox.min.css">
	<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH;?>/assets/slick/slick.css">
	<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH;?>/assets/slick/slick-theme.css">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<!--script src="<?=SITE_TEMPLATE_PATH;?>/js/jquery-3.2.1.min.js"></script-->
	<script src="<?=SITE_TEMPLATE_PATH;?>/js/jquery-migrate-1.4.1.min.js"></script>
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH;?>/assets/fancybox/jquery.fancybox.min.js"></script>
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH;?>/assets/slick/slick.min.js"></script>
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH;?>/assets/zoom-master/jquery.zoom.min.js"></script>
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH;?>/assets/jquery.maskedinput.min.js"></script>
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH;?>/js/script.js?<?=time();?>"></script>
</head>
<body>
	<?$APPLICATION->ShowPanel();?>
	<div class="popup_overlay"></div>
	<div class="svg_map"><? include('svg.php');?></div>
	<section class="head">
		<div class="head__container container">			
			<a href="tel:+74957858816" class="head__phone">+7(495)785 88 16</a>
			<a class="head__menu mob_el" href="javascript:void(0)">
				<svg width="21px" height="17px">
					<use xlink:href="#menu-sand"/>
				</svg>
			</a>
			<a href="/" class="top_logo">
				<svg class="head__logo" width="385" height="56">
					<use xlink:href="#main-logo" width="385" height="56"  />
				</svg>
				<svg class="head__logo mob_el head__logo_tab" width="296" height="45">
					<use xlink:href="#logo_tab" width="296" height="45"  />
				</svg>
				<svg class="head__logo mob_el head__logo_mob" width="184" height="27">
					<use xlink:href="#logo_mob" width="184" height="27"  />
				</svg>
			</a>
			<div class="head__cart">
				<a href="<?=($USER->IsAuthorized())?'/personal/':'/auth/';?>" class="head__cart_auth head__icon">
					<svg height="24" width="24">
						<use xlink:href="#personal"/>
					</svg>
				</a>
				<? if($USER->IsAuthorized()){?>				
				<!-- <a href="javascript:void(0)" class="head__cart_fav head__icon">
					<svg  height="24" width="24">
						<use xlink:href="#favorite"/>
					</svg>
				</a> -->
				<?}?>
				<?if($USER->IsAuthorized()){?>
				<a href="/personal/cart/" class="head__cart_cart head__icon">
					<span class="cart_count"><?=cartCount();?></span>
					<svg  height="24" width="24">
						<use xlink:href="#cart"/>
					</svg>
				</a>
				<a href="/personal/cart/" class="head__cart_cart_mob mob_el">
					<span class="cart_count"><?=cartCount();?></span>
					<svg  height="29" width="31">
						<use xlink:href="#cart_mob"/>
					</svg>
				</a>
				<?}?>
			</div>
			<div class="head__inout mob_el">
				<?if($USER->IsAuthorized()){?>
				<a href="/personal/">Личный кабинет</a>
				<!-- &nbsp;&nbsp;|&nbsp;&nbsp;<a href="javascript:void(0)">Избранное</a> -->
				<?}else{?>
				<a href="/auth/">Вход / Регистрация</a>
				<?}?>
			</div>
		</div>
	</section>
	<!-- /.head -->
	
	<?
	$APPLICATION->IncludeComponent(
			'nks:catalog.menu',
			'',
			array(
				'IBLOCK_ID' => 2,
				'BANNERS' => 4,
				'MAX_LEVEL' => 3
				),
			false
		);
	?>

	<?$APPLICATION->IncludeComponent(
		"bitrix:search.title", 
		"mars", 
		array(
			"NUM_CATEGORIES" => "1",
			"TOP_COUNT" => "5",
			"CHECK_DATES" => "Y",
			"SHOW_OTHERS" => "N",
			"PAGE" => SITE_DIR."catalog/",
			"CATEGORY_0_TITLE" => GetMessage("SEARCH_GOODS"),
			"CATEGORY_0" => array(
				0 => "iblock_catalog",
				1 => "iblock_offers",
			),
			"CATEGORY_0_iblock_catalog" => array(
				0 => "2",
			),
			"CATEGORY_OTHERS_TITLE" => GetMessage("SEARCH_OTHER"),
			"SHOW_INPUT" => "Y",
			"INPUT_ID" => "title-search-input",
			"CONTAINER_ID" => "top_search",
			"PRICE_CODE" => array(
				0 => "BASE",
			),
			"SHOW_PREVIEW" => "Y",
			"PREVIEW_WIDTH" => "75",
			"PREVIEW_HEIGHT" => "75",
			"CONVERT_CURRENCY" => "Y",
			"COMPONENT_TEMPLATE" => "mars",
			"ORDER" => "rank",
			"USE_LANGUAGE_GUESS" => "N",
			"PRICE_VAT_INCLUDE" => "N",
			"PREVIEW_TRUNCATE_LEN" => "",
			"CURRENCY_ID" => "RUB",
			"CATEGORY_0_iblock_offers" => array(
				0 => "3",
			)
		),
		false
	);?>

	<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "bc", array(
		"PATH" => "",	// Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
			"SITE_ID" => "s1",	// Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
			"START_FROM" => "0",	// Номер пункта, начиная с которого будет построена навигационная цепочка
			),
		false
		);?>
		<!-- /.bc_container container -->
	</section>
	<!-- /.bc -->	
