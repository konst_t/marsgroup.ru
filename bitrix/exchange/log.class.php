<?php

class LOGER
{
	public $log_folder = "logs/";
	private $log;

	public function Add($file) {
		if ($this->log=fopen($this->log_folder.$file, "w")) {
			return true;
		}
		return false;
	}

	public function Open($file) {
		if ($this->log=fopen($this->log_folder.$file, "a")) {
			return true;
		}
		return false;
	}

	public function PutLine($string) {
		return fputs($this->log,"[".date('d.m.Y H:i:s')."] ".$string."
");
	}

	public function Close() {
		return fclose($this->log);
	}

}

?>