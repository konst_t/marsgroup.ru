<?
require("settings.php");
require(ROOT_DIR."/bitrix/modules/main/include/prolog_before.php");
require("log.class.php");

use Bitrix\Main\Loader;

Loader::includeModule("catalog");
Loader::includeModule("sale");


$log = new LOGER();
$log->Open(file_get_contents("tmp/log"));                         
$log->PutLine("Начало обновления корзин");

// получение соответствий XML_ID и ID всех товаров

$db_res = CCatalogProduct::GetList();
while ($arProduct = $db_res->Fetch()) {
	$arProducts[$arProduct["ELEMENT_XML_ID"]] = $arProduct["ID"];
}
unset($db_res);                                                             //    echo "*****<pre>"; print_r($arProducts); echo "</pre>";    die();


// получаем пустые корзины

$bdBaskets = CSaleBasket::GetList(
	array(),
	array(
		'LID'=> SITE_ID,
		'ORDER_ID' => NULL
		),
	false,
	false,
	array("ID", "PRODUCT_XML_ID")
);																													$ww = 0;  $w = 0;
while ($baskets = $bdBaskets->GetNext()) {																			$w++; $ww++;
	$xmlID = $baskets["PRODUCT_XML_ID"];
	// если предложение получаем чистый xml_id
	if ( strrpos($baskets["PRODUCT_XML_ID"],'#') ) {
		$split = explode('#',$baskets["PRODUCT_XML_ID"]);
		$xmlID = $split[1];
	}                                                                                    //  echo $baskets["ID"]." - ".$arProducts[$xmlID]."<br>";
	if ( $arProducts[$xmlID]>0 ) {
		CSaleBasket::Update($baskets["ID"], Array("PRODUCT_ID"=>$arProducts[$xmlID]));
	}																												if($w==100) { $log->PutLine("Обновлено ".$ww." корзин"); $w = 0; }
}
if ( DEBUG ) {
	echo 'Корзины обновлены\r\n';
}
$log->PutLine("Обновление корзин закончено");

$log->PutLine("Начало очистки кэша");
//BXClearCache(true);


if (!class_exists("CFileCacheCleaner")) {
    require_once ($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/classes/general/cache_files_cleaner.php");
}
//Работаем со всем кешем
$obCacheCleaner = new CFileCacheCleaner("all");
$obCacheCleaner->InitPath($path);
$obCacheCleaner->Start();
while ($file = $obCacheCleaner->GetNextFile()) {
    if (is_string($file)) {
        unlink($file);
    }
}


if ( DEBUG ) {
	echo 'Кэш очищен\r\n';
}
$log->PutLine("Кэш очищен");

$log->Close();

die();

?>