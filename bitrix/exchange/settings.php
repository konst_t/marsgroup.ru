<?

// режим отладки (ручной переход по шагам)
define("DEBUG", false);
define("ROOT_DIR", "/var/www/marsgroup/data/www/marsgroup.ru");
// директория с файлами обмена
define("EXCHANGE_DIR", "/upload/1c_catalog/");
// имя файла архива
define("ARC_NAME", "exchange2.zip");
// директория внутри папки обмена
define("INNER_DIR", "000000004/");
// директория с фото
define("PICTURES_DIR", "/upload/1c_pictures/");
// папка со скриптами обмена
define("WORK_FOLDER", "/bitrix/exchange/");
// протокол сайта
define("PROTOCOL", "https://");
// ID инфоблока товаров
define("IBLOCK_ID", 2);
// ID инфоблока предложений
define("IBLOCK_ID_OFFERS", 3);
// ID инфоблока зеркала категорий
define("IBLOCK_ID_MIRROR", 5);
// влючать в лог ID успешно добавленых товаров
define("FULL_LOG", false);
// количество раз обмена файлы которых будут храниться на сервере
define("BACKUPS_QUANTITY", 6);


// XML_ID свойств товаров и предложений, которые мы не трогаем при
// обновлении списка свойств
$arConstantProperties = Array(
	"MORE_PHOTO",
	"CML2_LINK",
	"ARTNUMBER",
	"BREND_IBLOCK",
);

// свойства для фильтра
$arForFilter = Array(
	"STRANA_PROIZVODITEL",
	"BREND",
	"EKSKLYUZIV",
);

// наименования реквизитов, которые мы не считаем свойствами
$arNotPropDetails = Array(
//	"МодельРеквизит",
	"ОписаниеВФорматеHTML",
	"Полное наименование",
	"Категория",
	"ИндексСортировкиНоменклатуры",
);

// XML_ID свойств предложений
$arOfferProperties = Array(
	"2ef820bb-6f00-11e4-900b-000c292aad41",  //     Код ИМ
	"daa1c3b8-2e65-11e6-900d-000c292aad41",  //     Новинка
	"221c88db-6e55-11e4-900b-000c292aad41",  //     Эксклюзив
	"221c88dc-6e55-11e4-900b-000c292aad41",  //     Скидка
	"02e0d82a-c0f4-11e5-900c-000c292aad41",  //     Хит продаж
	"306c0002-cc18-11e5-900c-000c292aad41",  //     Акция
	"KOD",
//	"",
);

// типы цен
$arPriceTypes = [
	"b99de857-3bd7-11e7-9b37-000c292aad41" => 1,   //   основная
	"0a15d288-3845-11e6-900d-000c292aad41" => 3,   //   МРЦ
];

?>