<?
require("settings.php");
require(ROOT_DIR."/bitrix/modules/main/include/prolog_before.php");
require("class.php");
require("products.class.php");
require("log.class.php");

/*while(ob_get_level()) {
   ob_get_clean();
}*/

use Bitrix\Main\Loader;

Loader::includeModule("iblock");
Loader::includeModule("catalog");

$log = new LOGER();
$log->Open(file_get_contents("tmp/log"));
$log->PutLine("Начало ".$_GET["step"]." шага");

$full = Exchange::getExcangeType();

$exchange = new Products(true);

$log->PutLine("Шаг ".$exchange->step." окончен");
$log->Close();

if ( $exchange->bNotEmptyStep ) {
	$newstep = $exchange->step + 1;
	if ( DEBUG ) {
		echo '<br /><br />Шаг окончен. <a href="'.PROTOCOL.$_SERVER['HTTP_HOST'] .WORK_FOLDER.'products.php?step='.$newstep.'">Перейти к следующему</a>.';
	}
	else {
		sleep(5);
		exec('wget -b -q -O tmp/temp'.$newstep.'.php '.PROTOCOL.$_SERVER['HTTP_HOST'] .WORK_FOLDER.'products.php?step='.$newstep);
		die();
	}
}
else {
	if ($full) {
		if ( DEBUG ) {
			echo '<br /><br />Окончен последний шаг. <a href="'.PROTOCOL.$_SERVER['HTTP_HOST'] .WORK_FOLDER.'baskets.php">Обновить корзины</a>.';
		}
		else {
			exec('wget -b -q -O tmp/tempi'.$newstep.'.php '.PROTOCOL.$_SERVER['HTTP_HOST'] .WORK_FOLDER.'baskets.php');
			die();
		}
	}
}

?>