<?
require("settings.php");
require("class.php");
require("log.class.php");
require(ROOT_DIR."/bitrix/modules/main/include/prolog_before.php");

/*while(ob_get_level()) {
   ob_get_clean();
}*/

use Bitrix\Main\Loader;

Loader::includeModule("iblock");
Loader::includeModule("catalog");

$log = new LOGER();
$log->Open(file_get_contents("tmp/log"));

$log->PutLine("Начало обмена");

//if (!file_exists(ROOT_DIR.EXCHANGE_DIR.INNER_DIR."1")) {
if (!Exchange::getXML( "import" )) {
	Exchange::clean();
	$log->PutLine("Нет файлов для экспорта");
	$log->Close();
	die();
}

$exchange = new Exchange();

$log->PutLine("Шаг 0 окончен");
$log->Close();

if ( DEBUG ) {
	echo '<br /><br />Шаг окончен. <a href="'.PROTOCOL.$_SERVER['HTTP_HOST'] .WORK_FOLDER.'products.php?step=1">Перейти к следующему</a>.';
}
else {
	exec('wget -b -q -O tmp/temp1.php '.PROTOCOL.$_SERVER['HTTP_HOST'] .WORK_FOLDER.'products.php?step=1');
	die();
}

?>