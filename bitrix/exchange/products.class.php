<?

use \Bitrix\Main\IO\Directory as Directory;
use \Bitrix\Main\IO\File as File;
use Bitrix\Main\Application as Application;

class Products extends Exchange
{

	public $bNotEmptyStep = true;
	public $step;
	private $arGroupsAccordance = array();
	private $arModelsAccordance = array();
	private $arListProperties = array();
	private $arPictures = array();
	private $arProducts = array();
	private $arProductsWO = array();
	private $bFullExchange;

	function __construct( $products = false ) {
		global $log;
		$this->obLog = $log;
		// узнаем полный ли обмен
		$this->bFullExchange = self::getExcangeType();
		$this->arXML = self::getXML( "import" );
		if ( isset($_GET["step"]) ) {
			$this->step = $_GET["step"];
			$this->checkStep();
			if ( $this->step == 1 ) {
				file_put_contents("tmp/models", "");
			}
			else {
				$this->arModelsAccordance = unserialize(file_get_contents("tmp/models"));
			}
			if ( !$this->bNotEmptyStep ) {
				$this->closeExchange();
			}
		}
		else {
			return;
		}
		if ($this->bNotEmptyStep ) {
			// получаем соответствия параметров
			$this->getStartingArrays();
			$this->arXML = array();
			if ( $this->bFullExchange ) {      // полный обмен
				$this->getBrandsIblocks();
				$this->obLog->PutLine("Начинаю обновление товаров");
				$this->importProducts();
				$this->obLog->PutLine("Обновление товаров окончено");
				$this->arXML = array();
			}
			else {
				$this->getProductsAccordance();
			}
			$this->obLog->PutLine("Начинаю обновление остатков");
			$this->updateRests();
			$this->obLog->PutLine("Обновление остатков окончено");
			$this->arXML = array();
			$this->obLog->PutLine("Начинаю обновление цен");
			$this->updatePrices();
			$this->obLog->PutLine("Обновление цен окончено");
		}
		return;
	}

	/*
	проверка наличия файлов для шага
	*/
	public function checkStep() {
		if ( !file_exists($_SERVER["DOCUMENT_ROOT"].EXCHANGE_DIR.INNER_DIR.$this->step) || $this->step == "" ) {
			$this->bNotEmptyStep = false;
			if ( DEBUG ) { echo 'Файлы для шага не найдены'; }
			$this->obLog->PutLine("Экспорт окончен");
		}
		return;
	}

	/*
	получение массива соответствия ID и XML_ID из таблицы сайта для разделов
	*/
	private function getGroupsAccordancies() {
		$rs_section = CIBlockSection::GetList(array(),array("IBLOCK_ID"=>IBLOCK_ID));
		while($ar_section = $rs_section->Fetch()) {
			$this->arGroupsAccordance[$ar_section["XML_ID"]] = Array("ID" => $ar_section["ID"]);
		}
		unset($rs_section);                                                                     //   echo "<pre>"; print_r($this->arGroupsAccordance); echo "</pre>";
		return;
	}

	/*
	получение значений свойств типа список
	*/
	private function getListProperties() {
		foreach( $this->arPropertiesAccordance["products"] as $XML_ID => $prop ) {
			if ( $prop["PROPERTY_TYPE"] == "L" ) {
				$property_enums = CIBlockPropertyEnum::GetList(Array("ID"=>"ASC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>IBLOCK_ID, "CODE"=>$prop["CODE"]));
				while ($enum_fields = $property_enums->GetNext()) {
					$this->arListProperties["products"][$XML_ID][$enum_fields["XML_ID"]] = $enum_fields["ID"];
				}
			}
		}                                                                                 //     echo "<pre>"; print_r($this->arListProperties); echo "</pre>";
		foreach( $this->arPropertiesAccordance["offers"] as $XML_ID => $prop ) {
			if ( $prop["PROPERTY_TYPE"] == "L" ) {
				$property_enums = CIBlockPropertyEnum::GetList(Array("ID"=>"ASC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>IBLOCK_ID_OFFERS, "CODE"=>$prop["CODE"]));
				while ($enum_fields = $property_enums->GetNext()) {
					$this->arListProperties["offers"][$XML_ID][$enum_fields["XML_ID"]] = $enum_fields["ID"];
				}
			}
		}                                                                                 //     echo "<pre>"; print_r($this->arListProperties); echo "</pre>";
		return;
	}

	/*
	получение массива имен изображений
	*/
	private function getPictures() {
		$dir = $_SERVER["DOCUMENT_ROOT"].PICTURES_DIR;
		if (is_dir($dir)) {
			$dirHandle = opendir( $dir );
			while  (false !== ($file = readdir($dirHandle)) ) {
				$code = substr($file,0,11);
				$this->arPictures[$code][] = $file;
			}
		}	                                                                               // echo "<pre>"; print_r($this->arPictures); echo "</pre>";       die();
		return;
	}

	/*
	получение исходных массивов
	*/
	private function getStartingArrays() {
		$this->getPropertiesAccordancies();
		$this->getStoresAccordancies();
		$this->getGroupsAccordancies();
		$this->getListProperties();
		$this->getPictures();
		return;
	}

	/*
	импорт товаров
	*/
	private function importProducts() {
		$this->arXML = self::getXML( "import", $this->step."/" );                               // echo "<pre>"; print_r($this->arXML); echo "</pre>";      die();
		foreach( $this->arXML["Каталог"]["Товары"]["Товар"] as $key => $arProduct ) {
			// создаем массив свойств
			$arProperties = array();
			// свойства
			foreach( $arProduct["ЗначенияСвойств"]["ЗначенияСвойства"] as $property) {
				if ( is_array($property["Значение"]) ) {
					$property["Значение"] = "";
				}
				$arProperties[$property["Ид"]] = $property["Значение"];
				if ( $property["Ид"] == "BRAND_REF" ) {
					$arProduct["BREND_XML_ID"] = $property["Значение"];
				}
			}
			unset($arProduct["ЗначенияСвойств"]["ЗначенияСвойства"]);
			// реквизиты
			foreach( $arProduct["ЗначенияРеквизитов"]["ЗначениеРеквизита"] as $property ) {
				$XML_ID = CUtil::translit($property["Наименование"], "ru", Array("change_case"=>"U"));
				if ( is_array($property["Значение"]) ) {
					$property["Значение"] = "";
				}
				$arProperties[$XML_ID] = $property["Значение"];
			}
			unset($arProduct["ЗначенияРеквизитов"]["ЗначениеРеквизита"]);
			$arProduct["properties"] = $arProperties;
			// признак модели
			$model = $arProperties["MODELREKVIZIT"];
			if ( is_array($arProduct["Группы"]["Ид"]) ) {
				$arProduct["SECTION_XML_ID"] = $this->arGroupsAccordance[$arProduct["Группы"]["Ид"][0]]["ID"];
			}
			else {
				$arProduct["SECTION_XML_ID"] = $this->arGroupsAccordance[$arProduct["Группы"]["Ид"]]["ID"];
			}
			if ($model!="") {
				//if ( array_key_exists($model,$this->arModelsAccordance) ) {
				if ( isset($this->arModelsAccordance[$arProduct["SECTION_XML_ID"]][$model]) ) {
					$this->addOffer($arProduct);
				}
				else {
					$this->addProduct($arProduct,true);
				}
			}
			else {
				$this->addProduct($arProduct);
			}
			// убираем запись из массива для экономии памяти
			unset( $this->arXML["Каталог"]["Товары"]["Товар"][$key] );
		}
		file_put_contents("tmp/models", serialize($this->arModelsAccordance));           //    print_r($this->arModelsAccordance);
	}

	/*
	получение общих полей для товара и предложения
	*/
	private function getCommonFields($arProduct) {
		if ( DEBUG ) { /*echo $arProduct["Ид"]."<br />";*/ }
		if ( FULL_LOG ) { $this->obLog->PutLine($arProduct["Ид"]); }
		$code = CUtil::translit($arProduct["properties"]["KOD"], "ru", Array("change_case"=>"U"));
		$arMorePhotos = array();
		$detailPicture = false;
		if ( isset( $this->arPictures[$code] ) ) {
			$arPictures = $this->arPictures[$code];
			sort($arPictures);
			$n = 0;
			foreach( $arPictures as $pict ) {
				if ( substr_count( $pict, "_" ) ) {
					$arMorePhotos["n".$n] = Array("VALUE" => CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].PICTURES_DIR.$pict));
					$n++;
				}
				else {
					$detailPicture = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].PICTURES_DIR.$pict);
				}
			}
		}
		$arFields = Array(
			"ACTIVE"           => "Y",
			"XML_ID"           => $arProduct["Ид"],
			"DETAIL_TEXT_TYPE" => "html",
		);
		if ( $detailPicture ) { $arFields["DETAIL_PICTURE"] = $detailPicture; }
		if ( count($arMorePhotos) ) { $arFields["MORE_PHOTO"] = $arMorePhotos; }        //   echo "<pre>***"; print_r($arFields["MORE_PHOTO"]); echo "</pre>";
		return $arFields;
	}

	/*
	добавление товара
	*/
	private function addProduct($arProduct, $withOffers = false) {
		if ( $withOffers ) {
			$name = $arProduct["properties"]["KATEGORIYA"]." ".$arProduct["properties"]["BRENDREKVIZIT"]." ".$arProduct["properties"]["MODELREKVIZIT"];
		}
		else {
			$name = $arProduct["properties"]["POLNOE_NAIMENOVANIE"];
		}
		//$code = CUtil::translit($arProduct["properties"]["KOD"], "ru");
		$code = CUtil::translit($name, "ru");
		if ( is_array($arProduct["Группы"]["Ид"]) ) {
			$section = $this->arGroupsAccordance[$arProduct["Группы"]["Ид"][0]]["ID"];
		}
		else {
			$section = $this->arGroupsAccordance[$arProduct["Группы"]["Ид"]]["ID"];
		}
		if ( $arProduct["SECTION_XML_ID"] < 1 ) { return; }
		$arProps = array();
		foreach ( $this->arPropertiesAccordance["products"] as $XML_ID => $prop ) {
			if ( $prop["PROPERTY_TYPE"] == "L" ) {
				$arProps[$prop["CODE"]] = $this->arListProperties['products'][$XML_ID][$arProduct["properties"][$XML_ID]];
			}
			else {
				$arProps[$prop["CODE"]] = $arProduct["properties"][$XML_ID];
			}
		}
		$arFields = $this->getCommonFields($arProduct);
		if ( $section < 1 ) { $this->obLog->PutLine("Товар без категории<br />"); return; }
		$arFields["NAME"] = $name;
		$arFields["IBLOCK_ID"] = IBLOCK_ID;
		$arFields["CODE"] = $code;
		$arFields["IBLOCK_SECTION_ID"] = $section;
		$arFields["DETAIL_TEXT"] = $arProduct["properties"]["OPISANIEVFORMATEHTML"];
		$arProps["ARTNUMBER"] = $arProduct["Артикул"];
		$arProps["MORE_PHOTO"] = $arFields["MORE_PHOTO"];
		unset($arFields["MORE_PHOTO"]);
		$arProps["BREND_IBLOCK"] = $this->arBrandsIblocks[$arProduct["BREND_XML_ID"]];
		$arFields["PROPERTY_VALUES"] = $arProps;
		$arFields["PREVIEW_TEXT"] = $arProps["KOD"].', '.$arProps["ARTNUMBER"];
		if ( $arProduct["properties"]["INDEKSSORTIROVKINOMENKLATURY"] > 0 ) {
			$arFields["SORT"] = $arProduct["properties"]["INDEKSSORTIROVKINOMENKLATURY"];
		}
		$obElement = new CIBlockElement();                                                                         // echo "<pre>"; print_r($arFields); echo "</pre>";
		$ID = $obElement->Add($arFields);
		if ( DEBUG ) {
			if($ID<1) { echo $obElement->LAST_ERROR." -- ".$name."<br />"; }
			//else { echo "  --  ".$ID."<br />"; }
		}
		if ( FULL_LOG ) {
			if($ID<1) { $this->obLog->PutLine($obElement->LAST_ERROR." -- ".$name); }
			else { $this->obLog->PutLine("  --  ".$ID); }
		}
		else { if($ID<1) { $this->obLog->PutLine($obElement->LAST_ERROR."  --  ".$arProduct["Ид"]." -- ".$name); } }
		if($ID>0) {
			if ( $withOffers ) {
				$this->arProductsWO[$arProduct["Ид"]] = $ID;
				$this->arModelsAccordance[$arProduct["SECTION_XML_ID"]][$arProduct["properties"]["MODELREKVIZIT"]] = $ID;
				$this->addOffer($arProduct);
			}
			else {
				$this->arProducts[$arProduct["Ид"]] = $ID;
			}

		}
		return;
	}

	/*
	добавление предложения
	*/
	private function addOffer($arProduct) {
		global $arOfferProperties;
		$parentID = $this->arModelsAccordance[$arProduct["SECTION_XML_ID"]][$arProduct["properties"]["MODELREKVIZIT"]];
		if ( $parentID > 0 ) {   
			$arProps = array();
			foreach ( $arOfferProperties as $XML_ID ) {
				if ( $this->arPropertiesAccordance["offers"][$XML_ID]["PROPERTY_TYPE"] == "L" ) {
					$arProps[$this->arPropertiesAccordance["offers"][$XML_ID]["CODE"]] = $this->arListProperties["offers"][$XML_ID][$arProduct["properties"][$XML_ID]];
				}
				else {
					$arProps[$this->arPropertiesAccordance["offers"][$XML_ID]["CODE"]] = $arProduct["properties"][$XML_ID];
				}
			}                                                                                                                //      echo "<pre>"; print_r($arProps); echo "</pre>";
			$arProps["ARTNUMBER"] = $arProduct["Артикул"];
			$arProps["CML2_LINK"] = $parentID;
			$arFields = $this->getCommonFields($arProduct);
			$arFields["NAME"] = $arProduct["properties"]["POLNOE_NAIMENOVANIE"];
			$arFields["DETAIL_TEXT"] = $arProduct["properties"]["OPISANIEVFORMATEHTML"];
			$arFields["IBLOCK_ID"] = IBLOCK_ID_OFFERS;
			$arProps["ARTNUMBER"] = $arProduct["Артикул"];
			$arProps["MORE_PHOTO"] = $arFields["MORE_PHOTO"];
			unset($arFields["MORE_PHOTO"]);
			$arFields["PROPERTY_VALUES"] = $arProps;
			$obElement = new CIBlockElement();                                                                        //  echo "<pre>"; print_r($arFields); echo "</pre>";
			$ID = $obElement->Add($arFields);
			if($ID>0) { $this->arProducts[$arProduct["Ид"]] = $ID; }
			if ( DEBUG ) {
				if($ID<1) { echo $obElement->LAST_ERROR." -- ".$name."<br />"; }
				//else { echo "  --  ".$ID." -- offer<br />"; }
			}
			if ( FULL_LOG ) {
				if($ID<1) { $this->obLog->PutLine($obElement->LAST_ERROR." -- ".$name); }
				else { $this->obLog->PutLine("  --  ".$ID." -- offer"); }
			}
			else { if($ID<1) { $this->obLog->PutLine($obElement->LAST_ERROR."  --  ".$arProduct["Ид"]." -- ".$name); } }
			// обновление родителя
			$res = CIBlockElement::GetByID($parentID);
			$arParent = $res->GetNext();                                     
			$arFields = array();
			// признак новинки
			if ( $arProps["NOVINKA"] != '' ) {
				$arFields["NOVINKA"] = $this->arListProperties["products"]["daa1c3b8-2e65-11e6-900d-000c292aad41"]["daa1c3ba-2e65-11e6-900d-000c292aad41"];
			}
			// признак акции
			if ( $arProps["AKTSIYA"] != '' ) {
				$arFields["AKTSIYA"] = $this->arListProperties["products"]["306c0002-cc18-11e5-900c-000c292aad41"]["1a90ca4c-36d9-11e6-900d-000c292aad41"];
			}
			// признак скидки
			if ( $arProps["SKIDKA"] == $this->arListProperties["offers"]["221c88dc-6e55-11e4-900b-000c292aad41"]["true"] ) {
				$arFields["SKIDKA"] = $this->arListProperties["products"]["221c88dc-6e55-11e4-900b-000c292aad41"]["true"];
			}
			// признак эксклюзив
			if ( $arProps["EKSKLYUZIV"] == $this->arListProperties["offers"]["221c88db-6e55-11e4-900b-000c292aad41"]["true"] ) {
				$arFields["EKSKLYUZIV"] = $this->arListProperties["products"]["221c88db-6e55-11e4-900b-000c292aad41"]["true"];
			}
			// признак скидки
			if ( $arProps["KHIT_PRODAZH"] == $this->arListProperties["offers"]["02e0d82a-c0f4-11e5-900c-000c292aad41"]["true"] ) {
				$arFields["KHIT_PRODAZH"] = $this->arListProperties["products"]["02e0d82a-c0f4-11e5-900c-000c292aad41"]["true"];
			}
			if ( count($arFields) > 0 ) {
				CIBlockElement::SetPropertyValuesEx($parentID, false, $arFields);
			}
			$arFields = array();
			// добавление сортировки
			if ( $arProduct["properties"]["INDEKSSORTIROVKINOMENKLATURY"] > 0 ) {
				$arFields["SORT"] = $arProduct["properties"]["INDEKSSORTIROVKINOMENKLATURY"];
			}
			// обновление тегов
			$arFields["PREVIEW_TEXT"] = $arParent["PREVIEW_TEXT"].", ".$arProps["KOD"].', '.$arProps["ARTNUMBER"].", ".$arProduct["properties"]["POLNOE_NAIMENOVANIE"];
			$obElement->Update($parentID, $arFields);
			unset($arFields);
			unset($arProps);
		}
		else {
			if ( DEBUG ) { echo "Пустой ID родительского элемента<br />"; }
			$this->obLog->PutLine("Пустой ID родительского элемента  --  ".$ID);
		}
		return;
	}




	/*
	получение соответствий XML_ID и ID всех товаров
	*/
	private function getProductsAccordance() {
		// Получим список всех товаров
		$db_res = CCatalogProduct::GetList();
		while ($arProduct = $db_res->Fetch()) {
			if ( $arProduct["TYPE"] == 3 ) {
				$this->arProductsWO[$arProduct["ELEMENT_XML_ID"]] = $arProduct["ID"];
			}
			$this->arProducts[$arProduct["ELEMENT_XML_ID"]] = $arProduct["ID"];
		}
		unset($db_res);                                                              //   echo "<pre>"; print_r($this->arProductsWO); echo "</pre>"; //die();
		return;
	}

	/*
	обновление цен
	*/
	private function updatePrices() {                                                                          // echo "<pre>"; print_r($this->arProducts); echo "</pre>";    //  die();
		global $arPriceTypes;
		$this->arXML = self::getXML( "prices", $this->step."/" );                                              // echo "<pre>"; print_r($this->arXML); echo "</pre>";    //  die();
		if ( !$this->bFullExchange ) {
			$priceIDs = array();
			$priceIDs[1] = array();
			$res = CPrice::GetList( array(), array( "CATALOG_GROUP_ID" => 1 ) );
			while ($arr = $res->Fetch()) {
				$priceIDs[1][$arr["PRODUCT_ID"]] = $arr["ID"];
			}
			$priceIDs[3] = array();
			$res = CPrice::GetList( array(), array( "CATALOG_GROUP_ID" => 3 ) );
			while ($arr = $res->Fetch()) {
				$priceIDs[3][$arr["PRODUCT_ID"]] = $arr["ID"];
			}
		}
		foreach( $this->arXML["ПакетПредложений"]["Предложения"]["Предложение"] as $price ) {
			// общие параметры цены
			$currency = "RUB"; //str_replace("руб", "RUB", $price["Цены"]["Цена"]["Валюта"]);
			if(isset($price["Цены"]["Цена"]["ЦенаЗаЕдиницу"])) {
				$arFields = Array(
					"CURRENCY"         => $currency,
					"PRICE"            => $price["Цены"]["Цена"]["ЦенаЗаЕдиницу"],
					"CATALOG_GROUP_ID" => 1,
				);
				// если простой товар или предложение
				if ( isset( $this->arProducts[$price["Ид"]]) ) {
					$arFields["PRODUCT_ID"] = $this->arProducts[$price["Ид"]];
					if ( $this->bFullExchange ) {
						CPrice::Add( $arFields );
					}
					else {                                                                 //         echo $priceIDs[$this->arProducts[$price["Ид"]]]."<pre>"; print_r($arFields); echo "</pre>";
						if (!CPrice::Update( $priceIDs[1][$this->arProducts[$price["Ид"]]], $arFields )) echo "ошибка<br />";
					}
				}
				// если товар с предложениями
				if ( isset( $this->arProductsWO[$price["Ид"]]) ) {
					$arFields["PRODUCT_ID"] = $this->arProductsWO[$price["Ид"]];
					if ( $this->bFullExchange ) {
						CPrice::Add( $arFields );
					}
					else {                                                                 //  echo $priceIDs[$this->arProducts[$price["Ид"]]]."<pre>"; print_r($arFields); echo "</pre>";
						if (!CPrice::Update( $priceIDs[1][$this->arProductsWO[$price["Ид"]]], $arFields )) echo "ошибка<br />";
					}
				}
			}
			else {
				foreach($price["Цены"]["Цена"] as $pr) {
					if($pr["ЦенаЗаЕдиницу"] > 0) {
						$arFields = Array(
							"CURRENCY"         => $currency,
							"PRICE"            => $pr["ЦенаЗаЕдиницу"],
							"CATALOG_GROUP_ID" => $arPriceTypes[$pr["ИдТипаЦены"]],
						);
						// если простой товар или предложение
						if ( isset( $this->arProducts[$price["Ид"]]) ) {
							$arFields["PRODUCT_ID"] = $this->arProducts[$price["Ид"]];
							if ( $this->bFullExchange ) {
								CPrice::Add( $arFields );
							}
							else {                                                      //         echo $priceIDs[$this->arProducts[$price["Ид"]]]."<pre>"; print_r($arFields); echo "</pre>";
								if (!CPrice::Update( $priceIDs[$arPriceTypes[$pr["ИдТипаЦены"]]][$this->arProducts[$price["Ид"]]], $arFields )) echo "ошибка<br />";
							}
						}
						// если товар с предложениями
						if ( isset( $this->arProductsWO[$price["Ид"]]) ) {
							$arFields["PRODUCT_ID"] = $this->arProductsWO[$price["Ид"]];
							if ( $this->bFullExchange ) {
								CPrice::Add( $arFields );
							}
							else {                                                            //  echo $priceIDs[$this->arProducts[$price["Ид"]]]."<pre>"; print_r($arFields); echo "</pre>";
								if (!CPrice::Update( $priceIDs[$arPriceTypes[$pr["ИдТипаЦены"]]][$this->arProductsWO[$price["Ид"]]], $arFields )) echo "ошибка<br />";
							}
						}
					}
				}
			}
		}
		return;
	}

	/*
	обновление остатков
	*/
	private function updateRests() {
		if ( !$this->bFullExchange ) {
			$storeIDs = array();
			$res = CCatalogStoreProduct::GetList();
			while ($arr = $res->Fetch()) {
				$storeIDs[$arr["PRODUCT_ID"]][$arr["STORE_ID"]] = $arr["ID"];
			}
		}
		$this->arXML = self::getXML( "rests", $this->step."/" );
		$this->getStoresAccordancies();
		$stores = $this->arStoresAccordance;
		foreach( $this->arXML["ПакетПредложений"]["Предложения"]["Предложение"] as $rests ) {
			if ( isset( $this->arProducts[$rests["Ид"]]) ) {
				$total = 0;
				$arFieldsAll[] = array();
				foreach ( $rests["Остатки"]["Остаток"] as $rest ) {
					$arFieldsAll[] = Array(
						"PRODUCT_ID" => $this->arProducts[$rests["Ид"]],
						"STORE_ID"   => $stores[$rest["Склад"]["Ид"]]["ID"],
						"AMOUNT"     => $rest["Склад"]["Количество"],
					);
					$total = $total + $rest["Склад"]["Количество"];
				}
				if ( $this->bFullExchange ) {
					CCatalogProduct::add(array('ID' => $this->arProducts[$rests["Ид"]], 'QUANTITY' => $total));
				}
				else {
					CCatalogProduct::update($this->arProducts[$rests["Ид"]], array('QUANTITY' => $total));
				}
				foreach ( $arFieldsAll as $arFields ) {
					if (isset($arFields["PRODUCT_ID"])) {
						if ( $this->bFullExchange ) {
							$ID = CCatalogStoreProduct::Add($arFields);
						}
						else {
							$spID = $storeIDs[$arFields["PRODUCT_ID"]][$arFields["STORE_ID"]];
							CCatalogStoreProduct::Update($spID,$arFields);
						}
					}
				}
			}
		}
		return;
	}

		/*echo "после цен";
		$db_res = CCatalogProduct::GetList();
		while ($arProduct = $db_res->Fetch()) {  print_r($arProduct);  }*/

	/*
	очистка папок и файлов после обмена
	*/
	public function closeExchange() {
		// копируем файлы обмена и очищаем папеу обмена
		$from = Application::getDocumentRoot().EXCHANGE_DIR;
		$to = Application::getDocumentRoot().EXCHANGE_DIR."Reports/".str_replace(".log", "", file_get_contents("tmp/log"));
		// здесь все bool параметры должны быть true. Иначе это отладка
		CopyDirFiles( $from, $to, true, true, true, "Reports");
		// удаляем устаревшие архивные файлы обмена
		$stBackupsDir = Application::getDocumentRoot().EXCHANGE_DIR."Reports";
		$dircontent = scandir($stBackupsDir);
		$arFiles = array();
		foreach($dircontent as $filename) {
			if ($filename != '.' && $filename != '..') {
				if (filemtime($stBackupsDir."/".$filename) === false) {
					DeleteDirFilesEx($stBackupsDir."/".$file);
				}
				$dat = filemtime($stBackupsDir."/".$filename);
				$arFiles[$dat] = $filename;
			}
		}
		krsort($arFiles);
		$quantity = 1;
		foreach ( $arFiles as $file ) {
			if ( $quantity > BACKUPS_QUANTITY ) {
				Directory::deleteDirectory($stBackupsDir."/".$file);
			}
			$quantity++;
		}
		// удаляем устаревшие логи
		$stLogDir = Application::getDocumentRoot().WORK_FOLDER.$this->obLog->log_folder;
		$arFiles = array();
		$dircontent = scandir($this->obLog->log_folder);
		foreach($dircontent as $filename) {
			if ($filename != '.' && $filename != '..') {
				if (filemtime($this->obLog->log_folder.$filename) === false) {
					File::deleteFile( $stLogDir.$filename );
				}
				$dat = filemtime($this->obLog->log_folder.$filename);
				$arFiles[$dat] = $filename;
			}
		}
		krsort($arFiles);
		$quantity = 1;
		foreach ( $arFiles as $file ) {
			if ( $quantity > BACKUPS_QUANTITY ) {
				File::deleteFile( $stLogDir.$file );
			}
			$quantity++;
		}
		return;
	}

}

?>