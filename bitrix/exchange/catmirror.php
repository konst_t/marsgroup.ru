<?

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require("settings.php");

Bitrix\Main\Loader::includeModule("iblock");



function updateGroups( $parent = "", $arRes = Array() ) {	global $XML;	$bs = new CIBlockSection;
	// параметры для нулевого уровня
	if ( $parent == "" ) {
		$arRes = $XML["Классификатор"]["Группы"]["Группа"];
		//удаляем все из каталога
		$rs_section = CIBlockSection::GetList(array(),array("IBLOCK_ID"=>IBLOCK_ID_MIRROR));
		while($ar_section = $rs_section->Fetch()) {
			CIBlockSection::Delete($ar_section["ID"]);
		}
		unset($rs_section);
	}
	foreach ( $arRes as $arSection ) {		if ( $arSection["ПометкаУдаления"] != "true" ) {
			// добавляем раздел
			$arFields = Array(
				"IBLOCK_ID" => IBLOCK_ID_MIRROR,
				"IBLOCK_SECTION_ID" => $parent,
				"NAME" => $arSection["Наименование"],
				"XML_ID" => $arSection["Ид"],
				"CODE" => CUtil::translit($arSection["Наименование"], "ru"),
				"DESCRIPTION_TYPE" => "html",
			);
			$ID = $bs->Add($arFields);
			if($ID<1) echo $bs->LAST_ERROR;
			// делаем все то же рекурсивно для подгрупп
			if ( $ID > 0 && isset( $arSection["Группы"]["Группа"] ) && count( $arSection["Группы"]["Группа"] )>0 ) {				// случай когда группа одна и располагается непосредственно в $arSection["Группы"]["Группа"]
				if ( isset($arSection["Группы"]["Группа"]["Ид"]) && !is_array($arSection["Группы"]["Группа"]["Ид"]) && $arSection["Группы"]["Группа"]["Ид"] != ""  ) {
					$gr = $arSection["Группы"]["Группа"];
					$arSection["Группы"]["Группа"][0] = $gr;
					unset($arSection["Группы"]["Группа"]["Ид"]);
					unset($arSection["Группы"]["Группа"]["НомерВерсии"]);
					unset($arSection["Группы"]["Группа"]["ПометкаУдаления"]);
					unset($arSection["Группы"]["Группа"]["Наименование"]);
				}
				updateGroups( $ID, $arSection["Группы"]["Группа"] );			}
		}	}
}


$name = "import";
$path = $_SERVER["DOCUMENT_ROOT"].EXCHANGE_DIR;
if(file_exists($path) && is_dir($path)) {
	$dirHandle = opendir( $path );
	while  (false !== ($file = readdir($dirHandle)) ) {
		$cut = substr( $file, 0, strlen($name) );
		if ( !is_dir( $file ) && $cut == $name ) {
			$xml = simplexml_load_file( $path.$file );
			$json_string = json_encode( $xml );
			unset($xml);
			$XML = json_decode( $json_string, TRUE );
		}
	}
}
else {	echo "нету";}                    echo "<pre>"; print_r($XML); echo "</pre>*";    //  die();

updateGroups();




?>