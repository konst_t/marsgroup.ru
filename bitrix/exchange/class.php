<?

use Bitrix\Main\Application as Application;

class Exchange
{

	protected $arXML = Array();
	protected $arPropertiesAccordance = Array();
	protected $arStoresAccordance = Array();
	protected $obLog;
	protected $arBrandsIblocks = Array();
	private $arDetailsAccordance = Array();
	private $arGroupsSort = Array();
	private $catCreated = 0;


	function __construct( $products = false ) {
		global $log,$DB;
		$this->obLog = $log;
		// узнаем полный ли обмен
		$bFullExchange = self::getExcangeType();
		if ( $bFullExchange ) { $this->obLog->PutLine("Тип обмена: Полный обмен"); }
		else { $this->obLog->PutLine("Тип обмена: Быстрое обновление"); }
		if ( $bFullExchange ) {      // полный обмен
			// обновляем свойства, склады и группы
			$this->arXML = self::getXML( "import" );
			$this->getBrandsIblocks();
			$this->obLog->PutLine("Начинаю обновление свойств");
			$this->updateProperties();
			$this->obLog->PutLine("Начинаю обновление складов");
			$this->updateStores();
			$this->obLog->PutLine("Удаляю seo кэш");
			$DB->Query("TRUNCATE TABLE `b_iblock_element_iprop`");
			$this->obLog->PutLine("Начинаю обновление разделов");
			$this->arGroupsSort = $this->getGroupsSorts();
			$this->updateGroups();
			$this->obLog->PutLine("Создано ".$this->catCreated." категорий");
			$this->obLog->PutLine("Обновление разделов окончено");
			$this->arXML = Array();
			return;
		}
		return;
	}


	/*
	получаем тип обмена (true - полный/false - изменения)
	*/
	public static function getExcangeType() {
		$arXml = self::getXML( "offers" );
		$type = ($arXml["ПакетПредложений"]["@attributes"]["СодержитТолькоИзменения"] == "true" ? false : true);
		unset($arXml);
		return $type;
	}

	/*
	получаем массив из XML файла
	$dir - директория внутри директории обмена. Начинается без "/", заканчивается "/". Например, "1/" Корень "".
	$name - постоянная часть имени файла (начало имени)
	*/
	public static function getXML( $name, $dir = "" ) {
		$path = Application::getDocumentRoot().EXCHANGE_DIR.INNER_DIR.$dir;
		if(file_exists($path) && is_dir($path)) {
			$dirHandle = opendir( $path );
			while  (false !== ($file = readdir($dirHandle)) ) {
				$cut = substr( $file, 0, strlen($name) );
				if ( !is_dir( $file ) && $cut == $name ) {
					$xml = simplexml_load_file( $path.$file );
					$json_string = json_encode( $xml );
					unset($xml);
					return json_decode( $json_string, TRUE );
				}
			}
		}
		return false;
	}

	/*
	Очистка директории обмена
	*/
	public static function clean() {
		$dir = Application::getDocumentRoot().EXCHANGE_DIR;
		$dircontent = scandir($dir);
		foreach($dircontent as $filename) {
			if ( $filename != "Reports" && $filename != "." && $filename != "..") {
				\Bitrix\Main\IO\Directory::deleteDirectory($dir.$filename);
			}
		}
	}

	/*
	получаем бренды из базы
	*/
	protected function getBrandsIblocks() {
		$rsData = CIBlockElement::GetList(array(), array("IBLOCK_ID" => 7 ), array("ID", "XML_ID", "NAME"));
		while($arData = $rsData->GetNext()) {
			$this->arBrandsIblocks[$arData["XML_ID"]] = $arData["ID"];
		}
	}

	/*
	получаем соответствие наименования реквизитов их кодам
	*/
	private function getDetailsAccordancies() {
		//берем из XML товаров все имена реквизитов
		$arXml = self::getXML( "import", "1/" );
		$arDetails = ($arXml["Каталог"]["Товары"]["Товар"][0]["ЗначенияРеквизитов"]["ЗначениеРеквизита"]);
		unset($arXml);
		foreach( $arDetails as $detail ) {
			$this->arDetailsAccordance[$detail["Наименование"]] = CUtil::translit($detail["Наименование"], "ru", Array("change_case"=>"U"));
		}
		return;
	}

	/*
	получаем из базы магазина соответствие кода и ID свойства XML_ID
	*/
	protected function getPropertiesAccordancies() {
		$result = Array();
		$res = CIBlock::GetProperties(IBLOCK_ID);
		while($res_arr = $res->Fetch()){
			$result["products"][$res_arr["XML_ID"]] = Array("ID"=>$res_arr["ID"],"CODE"=>$res_arr["CODE"],"PROPERTY_TYPE"=>$res_arr["PROPERTY_TYPE"]);
		}
		unset($res);
		$res = CIBlock::GetProperties(IBLOCK_ID_OFFERS);
		while($res_arr = $res->Fetch()){
			$result["offers"][$res_arr["XML_ID"]] = Array("ID"=>$res_arr["ID"],"CODE"=>$res_arr["CODE"],"PROPERTY_TYPE"=>$res_arr["PROPERTY_TYPE"]);
		}
		unset($res);                                           //echo "<pre>"; print_r($result); echo "</pre>";
		$this->arPropertiesAccordance = $result;
		return;
	}

	/*
	обработка свойства
	*/
	private function handleProperty($prop,$XML_ID,$mode) {
		// Если есть свойство
		if ( isset($this->arPropertiesAccordance[$mode][$XML_ID]) ) {
			// если справочник, то проверяем значения
			if ( $prop["ТипЗначений"] == "Справочник" ) {
				$ibp = new CIBlockProperty;
				$arFields["VALUES"] = $this->getPropertyList($prop["ВариантыЗначений"]["Справочник"]);
				$ibp->Update($this->arPropertiesAccordance[$mode][$XML_ID]["ID"], $arFields);
				if ( $prop["Ид"] == "BRAND_REF" ) {
					foreach( $prop["ВариантыЗначений"]["Справочник"] as $brand ) {
						if (!isset($this->arBrandsIblocks[$brand["ИдЗначения"]])) {
							$arFields = Array(
								"IBLOCK_ID" => 7,
								"NAME"      => $brand["Значение"],
								"XML_ID"    => $brand["ИдЗначения"],
								"ACTIVE"    => "N",
							);
							$obElement = new CIBlockElement();
							$ID = $obElement->Add($arFields);
						}
					}
				}
			}
			// убиваем элемент массива arPropertiesAccordance с этим кодом
			unset($this->arPropertiesAccordance[$mode][$XML_ID]);
			return false;
		}
		// Если нет, то добавляем свойство.
		else {
			if ( $prop["ТипЗначений"] == "Справочник" ) {
				$arFields["PROPERTY_TYPE"] = "L";
				$arFields["VALUES"] = $this->getPropertyList($prop["ВариантыЗначений"]["Справочник"]);
			}
			else {
				$arFields["PROPERTY_TYPE"] = "S";
			}
			return $arFields;
		}
	}

	/*
	получение значений списков
	*/
	private function getPropertyList($list) {
		$arFields["VALUES"] = Array();
		// случай когда значение одно и располагается непосредственно в $prop["ВариантыЗначений"]["Справочник"]
		if ( isset($list["ИдЗначения"]) && !is_array($list["ИдЗначения"]) && $list["ИдЗначения"] != ""  ) {
			$arFields["VALUES"][] = Array(
				"XML_ID" => $list["ИдЗначения"],
				"VALUE"  => $list["Значение"],
			);
		}
		// обычный случай
		else {
			foreach( $list as $value ) {
				//если свойства расположены в массиве
				if ( is_array($value) ) {
					if ( !is_array($value["ИдЗначения"]) && $value["ИдЗначения"] != "" ) {
						$arFields["VALUES"][] = Array(
							"XML_ID" => $value["ИдЗначения"],
							"VALUE"  => $value["Значение"],
						);
					}
				}
			}
		}
		return $arFields["VALUES"];
	}

	/*
	обновление списка свойств инфоблоков
	*/
	private function updateProperties() {
		global $arConstantProperties, $arNotPropDetails, $arOfferProperties, $arForFilter;
		$arXML_ID = Array();
		$arFields = Array(
			"ACTIVE" => "Y",
			"SEARCHABLE" => "Y",
			"FILTRABLE" => "Y"
		);
		$this->getPropertiesAccordancies();
		$ibp = new CIBlockProperty;
		// СВОЙСТВА
		//берем из XML все свойств и прогоняем в цикле
		foreach( $this->arXML["Классификатор"]["Свойства"]["Свойство"] as $prop ) {
			$XML_ID = $prop["Ид"];
			// добавляем данные на случай если нужно будет создать свойство
			$arFields["NAME"] = $prop["Наименование"];
			$arFields["CODE"] = CUtil::translit($prop["Наименование"], "ru", Array("change_case"=>"U"));
			$arFields["XML_ID"] = $XML_ID;
			if ( in_array($arFields["CODE"],$arForFilter) ) {
				$arFields["SMART_FILTER"] = "Y";
			}
			if ( $prop["ПометкаУдаления"] != "true" ) {
				$arFields["IBLOCK_ID"] = IBLOCK_ID;
				$arFields_ = $this->handleProperty($prop,$XML_ID,"products");
				if (is_array($arFields_)) {
					if ( $prop["ТипЗначений"] != "Справочник" ) {
						unset($arFields["VALUES"]);
					}
					$arFields = array_merge($arFields, $arFields_);         //  echo "<pre>"; print_r($arFields); echo "</pre>";
					$PropID = $ibp->Add($arFields);
				}
				if ( in_array($XML_ID, $arOfferProperties) ) {
					$arFields["IBLOCK_ID"] = IBLOCK_ID_OFFERS;
					$arFields_ = $this->handleProperty($prop,$XML_ID,"offers");
					if (is_array($arFields_)) {
						if ( $prop["ТипЗначений"] != "Справочник" ) {
							unset($arFields["VALUES"]);
						}
						$arFields = array_merge($arFields, $arFields_);        //  echo "<pre>"; print_r($arFields); echo "</pre>";
						$PropID = $ibp->Add($arFields);
					}
				}
			}
		}
		// РЕКВИЗИТЫ
		//получаем массив реквизитов
		$this->getDetailsAccordancies();
		// в цикле идем по реквизитам
		foreach( $this->arDetailsAccordance as $name => $XML_ID ) {
			// если реквизит с таким наименованием не должен добавляться как свойство, то пропускаем его
			if ( in_array( $name, $arNotPropDetails ) ) { continue; }
			// добавляем данные на случай если нужно будет создать свойство
			$arFields["NAME"] = $name;
			$arFields["CODE"] = $XML_ID;
			$arFields["XML_ID"] = $XML_ID;
			$arFields["PROPERTY_TYPE"] = "S";
			unset($arFields["VALUES"]);
			if ( in_array($arFields["CODE"],$arForFilter) ) {
				$arFields["SMART_FILTER"] = "Y";
			}
			// Если есть свойство, то убиваем элемент массива arPropertiesAccordance с этим кодом
			if ( isset($this->arPropertiesAccordance["products"][$XML_ID]) ) {
				unset($this->arPropertiesAccordance["products"][$XML_ID]);
			}
			// Если нет, то добавляем свойство.
			else {
				$arFields["IBLOCK_ID"] = IBLOCK_ID;
				$PropID = $ibp->Add($arFields);
			}
			// смотрим должно ли это свойство быть у предложений
			if ( in_array($XML_ID, $arOfferProperties) ) {
				// Если есть свойство, то убиваем элемент массива arPropertiesAccordance с этим кодом
				if ( isset($this->arPropertiesAccordance["offers"][$XML_ID]) ) {
					unset($this->arPropertiesAccordance["offers"][$XML_ID]);
				}
				// Если нет, то добавляем свойство.
				else {
					$arFields["IBLOCK_ID"] = IBLOCK_ID_OFFERS;
					$PropID = $ibp->Add($arFields);
				}
			}
		}
		//удаляем все оставшиеся в arPropertiesAccordance свойства кроме указанных в $arConstantProperties
		foreach ( $this->arPropertiesAccordance["products"] as $key => $prop ) {
			if ( !in_array($prop['CODE'],$arConstantProperties) ) {
				$ibp->Delete($prop['ID']);
			}
		}
		foreach ( $this->arPropertiesAccordance["offers"] as $key => $prop ) {
			if ( !in_array($prop['CODE'],$arConstantProperties) ) {
				$ibp->Delete($prop['ID']);
			}
		}
		$this->arPropertiesAccordance = Array();
		$this->obLog->PutLine("Обновление свойств окончено");
	}



	/*
	получаем сортировку каталога
	*/
	private function getGroupsSorts() {
		$rs_section = CIBlockSection::GetList(array(),array("IBLOCK_ID"=>IBLOCK_ID_MIRROR));
		while($ar_section = $rs_section->Fetch()) {
			$arSort[$ar_section["XML_ID"]] = $ar_section["SORT"];
		}
		unset($rs_section);                                                              //   echo "***<pre>"; print_r($arSort); echo "</pre>";   die();
		return $arSort;
	}

	/*
	обновление разделов каталога
	*/
	private function updateGroups( $parent = "", $arRes = Array() ) {
		$bs = new CIBlockSection;
		// параметры для нулевого уровня
		if ( $parent == "" ) {
			$arRes = $this->arXML["Классификатор"]["Группы"]["Группа"];
			//удаляем все из каталога
			$rs_section = CIBlockSection::GetList(array(),array("IBLOCK_ID"=>IBLOCK_ID));
			while($ar_section = $rs_section->Fetch()) {
				CIBlockSection::Delete($ar_section["ID"]);
			}
			unset($rs_section);
			// убиваем все товары и предложения, которые могли остаться
			$result = CIBlockElement::GetList(
				array("ID"=>"ASC"),
				array(
					'IBLOCK_ID'=>IBLOCK_ID,
				)
			);
			while($element = $result->Fetch())
				CIBlockElement::Delete($element['ID']);
			$result = CIBlockElement::GetList(
				array("ID"=>"ASC"),
				array(
					'IBLOCK_ID'=>IBLOCK_ID_OFFERS,
				)
			);
			while($element = $result->Fetch()) {
				CIBlockElement::Delete($element['ID']);
			}
			$this->obLog->PutLine("Каталог очищен");
		}
		foreach ( $arRes as $arSection ) {
			if ( $arSection["ПометкаУдаления"] != "true" ) {
				// добавляем раздел
				$sort = ( $this->arGroupsSort[$arSection["Ид"]] > 0 ? $this->arGroupsSort[$arSection["Ид"]] : 500 );      //echo $arSection["Ид"]." - ".$sort;
				$arFields = Array(
					"IBLOCK_ID"         => IBLOCK_ID,
					"IBLOCK_SECTION_ID" => $parent,
					"NAME"              => $arSection["Наименование"],
					"XML_ID"            => $arSection["Ид"],
					"CODE"              => CUtil::translit($arSection["Наименование"], "ru"),
					"DESCRIPTION_TYPE"  => "html",
					"SORT"              => $sort,
				);
				$ID = $bs->Add($arFields);
				if($ID<1) echo $bs->LAST_ERROR;
				// делаем все то же рекурсивно для подгрупп
				if ( $ID > 0 && isset( $arSection["Группы"]["Группа"] ) && count( $arSection["Группы"]["Группа"] )>0 ) {
					$this->catCreated++;
					// случай когда группа одна и располагается непосредственно в $arSection["Группы"]["Группа"]
					if ( isset($arSection["Группы"]["Группа"]["Ид"]) && !is_array($arSection["Группы"]["Группа"]["Ид"]) && $arSection["Группы"]["Группа"]["Ид"] != ""  ) {
						$gr = $arSection["Группы"]["Группа"];
						$arSection["Группы"]["Группа"][0] = $gr;
						unset($arSection["Группы"]["Группа"]["Ид"]);
						unset($arSection["Группы"]["Группа"]["НомерВерсии"]);
						unset($arSection["Группы"]["Группа"]["ПометкаУдаления"]);
						unset($arSection["Группы"]["Группа"]["Наименование"]);
					}
					$this->updateGroups( $ID, $arSection["Группы"]["Группа"] );
				}
			}
		}
	}



	/*
	получение массива соответствия ID и XML_ID из таблицы сайта для складов
	*/
	protected function getStoresAccordancies() {
		$rsStores = CCatalogStore::GetList();
		while($arStore = $rsStores->Fetch()) {
			$this->arStoresAccordance[$arStore["XML_ID"]] = Array("ID" => $arStore["ID"]);
		}
		unset($rsStores);                                                                      //  echo "<pre>"; print_r($this->arStoresAccordance); echo "</pre>";
		return;
	}

	/*
	обновление всех слдадов
	*/
	private function updateStores() {
		$this->getStoresAccordancies();
		// убиваем все склады
		foreach( $this->arStoresAccordance as $store ) {
			CCatalogStore::Delete($store["ID"]);
		}
		// добавляем склады из XML
		foreach( $this->arXML["Классификатор"]["Склады"]["Склад"] as $store ) {
			// добавляем склад
			$arFields = Array(
				"TITLE" => $store["Наименование"],
				"ACTIVE" => "Y",
				"XML_ID" => $store["Ид"],
			);
			$ID = CCatalogStore::Add($arFields);
		}
		$this->obLog->PutLine("Обновление складов окончено");
	}

}

?>