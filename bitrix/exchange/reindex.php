<?
require("settings.php");
require(ROOT_DIR."/bitrix/modules/main/include/prolog_before.php");
require("log.class.php");

use Bitrix\Main\Loader;

Loader::includeModule("iblock");
Loader::includeModule("catalog");
if (!Loader::includeModule('search')) {
    die('Search module not included');
}

$log = new LOGER();
$log->Open(file_get_contents("tmp/log"));
$log->PutLine("Начало переиндексации поиска");

if (!CModule::IncludeModule('search')) {
	die('Search module not included');
}

$time_start = time();

CSearch::ReindexModule("iblock",true);

$total_time = time() - $time_start;

if ( DEBUG ) {
	echo "reindex finished. total time: " . $total_time . " seconds\r\n";
}

$log->PutLine("Индексация закончена");
$log->Close();

die();

?>