<?
require("settings.php");
require("log.class.php");
require(ROOT_DIR."/bitrix/modules/main/include/prolog_before.php");

function ClearDir($path){
	if(file_exists($path) && is_dir($path)) {
		$dirHandle = opendir($path);
		while (false !== ($file = readdir($dirHandle))) {
			if ($file!='.' && $file!='..') {
				$tmpPath=$path.DIRECTORY_SEPARATOR.$file;
				if (is_dir($tmpPath)) {
					RemoveDir($tmpPath);
				}
				else {
					if(file_exists($tmpPath)) {
						unlink($tmpPath);
					}
				}
			}
		}
		closedir($dirHandle);
		return true;
	}
	return false;
}

function RemoveDir($path){
	if(ClearDir($path)) {
		rmdir($path);
		return true;
	}
	return false;
}

$log = new LOGER();
$logFileName = date('d_m_Y_H_i_s').".log";
$log->Add($logFileName);
file_put_contents("tmp/log", $logFileName);

// Исходный архив
$sFilePathArc = ROOT_DIR.EXCHANGE_DIR.ARC_NAME;
// Директория для распаковки
$sFilePathDst = ROOT_DIR.EXCHANGE_DIR;
// Параметры распаковки
/*$arUnpackOptions = Array(
   "REMOVE_PATH"      => ROOT_DIR,
   "UNPACK_REPLACE"   => true
);*/


if ( file_exists($sFilePathArc) ) {
	$log->PutLine("Разархивация");
	// Удаляем если что-то осталось от предидущих обменов
	RemoveDir(ROOT_DIR.EXCHANGE_DIR.INNER_DIR);
	// Магия


$zip = new ZipArchive;
if ($zip->open($sFilePathArc) === TRUE) {
	// путь к каталогу, в который будут помещены файлы
	$zip->extractTo($sFilePathDst);
	$zip->close();
	unlink($sFilePathArc);

	if ( DEBUG ) {
		echo '<br /><br />Архив распакован. <a href="'.PROTOCOL.$_SERVER['HTTP_HOST'] .WORK_FOLDER.'structure.php">Начать обмен</a>.';
	}
	else {
		exec('wget -b -q -O tmp/temp.php '.PROTOCOL.$_SERVER['HTTP_HOST'] .WORK_FOLDER.'structure.php');
		die();
	}
} else {
	if ( DEBUG ) {
		echo "Файл не распакован";
	}
	$log->PutLine("Файл не распакован");
	die();
}

	/*
	$resArchiver = CBXArchive::GetArchive($sFilePathArc);
	$resArchiver->SetOptions($arUnpackOptions);
	$uRes = $resArchiver->Unpack($sFilePathDst);
	// Вывод ошибки или результата
	if (!$uRes) {
		if ( DEBUG ) {
			echo $resArchiver->GetErrors()[0];
		}
		$log->PutLine($resArchiver->GetErrors()[0]);
		die();
	} else {
		unlink($sFilePathArc);
		if ( DEBUG ) {
			echo '<br /><br />Архив распакован. <a href="'.PROTOCOL.$_SERVER['HTTP_HOST'] .WORK_FOLDER.'structure.php">Начать обмен</a>.';
		}
		else {
			exec('wget -b -q -O tmp/temp.php '.PROTOCOL.$_SERVER['HTTP_HOST'] .WORK_FOLDER.'structure.php');
			die();
		}
	}*/
}
elseif ( is_dir(ROOT_DIR.EXCHANGE_DIR.INNER_DIR) ) {
		if ( DEBUG ) {
			echo '<br /><br />Файлы уже распакованы. <a href="'.PROTOCOL.$_SERVER['HTTP_HOST'] .WORK_FOLDER.'structure.php">Начать обмен</a>.';
		}
		else {
			exec('wget -b -q -O tmp/temp.php '.PROTOCOL.$_SERVER['HTTP_HOST'] .WORK_FOLDER.'structure.php');
			die();
		}
}
else {
	if ( DEBUG ) {
		echo "Файлы не найдены.";
	}
	$log->PutLine("Файлы не найдены.");
}


?>
