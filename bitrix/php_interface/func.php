<?
CModule::IncludeModule('iblock');
CModule::IncludeModule('sale');
define('NKS_CATALOG',2);
define('NKS_OFFERS',3);

function setResize($id,$w = 200,$h = 200,$t = 1){
	$type = ($t == 1)?BX_RESIZE_IMAGE_PROPORTIONAL:BX_RESIZE_IMAGE_EXACT;
	$_img = CFile::ResizeImageGet($id, array("width"=>$w, "height"=>$h),$type);
	return $_img['src'];
}
function setResizeArray($id,$s = array(200,200),$m = array(),$b = array(),$size = false, $t = 1){
	$res = array();
	$type = ($t == 1)?BX_RESIZE_IMAGE_PROPORTIONAL:BX_RESIZE_IMAGE_EXACT;
	$_img = CFile::ResizeImageGet($id, array("width"=>$s[0], "height"=>$s[1]),$type,$size);	
	$res['SM'] = $_img['src'];
	if ($size) {
		$res['SM_W'] = $_img['width'];
		$res['SM_H'] = $_img['height'];
	}
	if (count($m)) {
		$_img = CFile::ResizeImageGet($id, array("width"=>$m[0], "height"=>$m[1]),$type,$size);
		$res['MD'] = $_img['src'];
		if ($size) {
			$res['MD_W'] = $_img['width'];
			$res['MD_H'] = $_img['height'];
		}
	}
	if (count($b)) {
		$_img = CFile::ResizeImageGet($id, array("width"=>$b[0], "height"=>$b[1]),$type,$size);
		$res['LG'] = $_img['src'];
		if ($size) {
			$res['LG_W'] = $_img['width'];
			$res['LG_H'] = $_img['height'];
		}
	}else{
		$res['BIG'] = CFile::GetPath($id);
	}
	return $res;
}
function cartCount(){
	$cntBasketItems = CSaleBasket::GetList(
	   array(),
	   array( 
	      "FUSER_ID" => CSaleBasket::GetBasketUserID(),
	      "LID" => SITE_ID,
	      "ORDER_ID" => "NULL",
	      "!CAN_BUY" => "N"
	   ), 
	   array()
	);
	return $cntBasketItems;
}
/*function cartCount(){
	$cntBasketItems = CSaleBasket::GetList(
	   array(),
	   array( 
	      "FUSER_ID" => CSaleBasket::GetBasketUserID(),
	      "LID" => SITE_ID,
	      "ORDER_ID" => "NULL",
	   )
	);
	ob_start();
	while ( $xx = $cntBasketItems->Fetch()) {
		echo '<pre>';
		print_r($xx);
		echo '</pre><hr/>';
	}	
	$res = ob_get_clean();
	return $res;
}*/
function upFav($val,$arr){
	$flip = array_flip($arr);
	if(array_key_exists($val,$flip)){
	    unset($flip[$val]);
	    $arr = array_flip($flip);
	}else{
		$arr[$val] = $val;
	}
	return $arr;
}