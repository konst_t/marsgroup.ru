<?php

use Bitrix\Main;


$eventManager = Main\EventManager::getInstance();

require_once('func.php');

if (file_exists($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/include/upload_clean_agent.php"))
	require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/include/upload_clean_agent.php");

if (file_exists($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/include/rates_parser.php"))
	require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/include/rates_parser.php"); /**/


// Письмо пользователю при оформлении нового заказа
	AddEventHandler("sale", "OnOrderNewSendEmail", Array("mail_new", "OnOrderAdd_mail"));

	class mail_new
	{

		function OnOrderAdd_mail($ID, $ev, $val)
		{
			Cmodule::IncludeModule("catalog");
			Cmodule::IncludeModule("iblock");
			Cmodule::IncludeModule("sale");
			$arOrder = CSaleOrder::GetByID($ID);
			$strOrderList = "";
			$arBasketList = array();
			$dbBasketItems = CSaleBasket::GetList(
					array("ID" => "ASC"),
					array("ORDER_ID" => $ID),
					false,
					false,
					array()
				);
			while ($arItem = $dbBasketItems->Fetch())
			{
				$arBasketList[] = $arItem;
			}

			if (!empty($arBasketList) && is_array($arBasketList))
			{

				$strOrderList .= '<table cellspacing="2" cellpadding="2" border="1">';
				$strOrderList .= '<tr>
				<td>Код</td>
				<td>Название</td>
				<td>Количество</td>
				<td>Цена</td>
				</tr>';
				foreach ($arBasketList as $arItem)
				{
					$code_tov = '';
					//$db_props =  CIBlockElement::GetProperty (25, $arItem['PRODUCT_ID'], array("sort" => "asc"),  array("CODE"=>"KOD_ATTR_S"));
					$db_props = CIBlockElement::GetList(Array(), Array("ID" => $arItem['PRODUCT_ID']), Array("PROPERTY_KOD"));
					while($ar_props = $db_props->Fetch()){
						$code_tov = $ar_props["PROPERTY_KOD_VALUE"];
					}
					$strOrderList .='<tr>';
					$strOrderList .= '<td>'.$code_tov.'</td>';
					$strOrderList .= '<td><a href="http://www.marsgroup.ru'.$arItem["DETAIL_PAGE_URL"].'">'.htmlspecialchars($arItem["NAME"]).'</a></td>';
					$strOrderList .= '<td>'.htmlspecialchars($arItem["QUANTITY"]).'</td>';
					$strOrderList .= '<td>'.SaleFormatCurrency($arItem["PRICE"], $arItem["CURRENCY"]).'</td>';
					$strOrderList .= "</tr>";

				}
				$strOrderList .= '</table>';
				$strOrderList .= '<div style="padding:20px 0"><b>Комментарии:</b><br>';
				$strOrderList .= $arOrder['USER_DESCRIPTION'];
				$strOrderList .= '</div>';

			}
			global $USER;
			$_usr = $USER->GetByLogin($arOrder['USER_LOGIN']);
			$ORDER_USER = '';
			if ($usr = $_usr->GetNext()) {
				$ORDER_USER .= '<ul>';
				$ORDER_USER .= '    <li>Компания: '.$usr['WORK_COMPANY'].'</li>';
				$ORDER_USER .= '    <li>ИНН: '.$usr['UF_INN'].'</li>';
				$ORDER_USER .= '    <li>Логин: '.$arOrder['USER_LOGIN'].'</li>';
				$ORDER_USER .= '    <li>Email: '.$usr['EMAIL'].'</li>';
				$ORDER_USER .= '    <li>Телефон: '.$usr['PERSONAL_PHONE'].'</li>';
				$ORDER_USER .= '</ul>';
			}else{
				$ORDER_USER = $val['ORDER_USER'];
			}


			$arFields = array(
				"ORDER_ID" => $ID,
				"ORDER_DATE" => $val['ORDER_DATE'],
				"ORDER_USER" => $ORDER_USER,
				"PRICE" => $val["PRICE"],
				//"BCC" => COption::GetOptionString("sale", "order_email", "order@".$SERVER_NAME),
				"EMAIL" => $val['EMAIL'],
				"ORDER_LIST" => $strOrderList,
				"SALE_EMAIL" => COption::GetOptionString("sale", "order_email", "order@".$SERVER_NAME)
			);

			$eventName = "SALE_NEW_ORDER_2";
			$event = new CEvent;
			$event->Send($eventName, SITE_ID, $arFields, "N");

			return false;
		}

	}


/*AddEventHandler("search", "OnAfterIndexAdd", Array("TestReindex", "AddSearchExtData"));

class TestReindex{
function AddSearchExtData($ID, $arFields)
	{
		file_put_contents("/var/www/www-root/data/www/marsgroup.ru/ritest.txt", "*", FILE_APPEND);
	}
}*/


AddEventHandler("main", "OnBeforeUserUpdate", Array("UserUpdateHandler", "OnBeforeUserUpdateHandler"));
AddEventHandler("main", "OnAfterUserUpdate", Array("UserUpdateHandler", "OnAfterUserUpdateHandler"));

class UserUpdateHandler
{
	// создаем обработчик события "OnBeforeUserUpdate"
	function OnBeforeUserUpdateHandler(&$arFields)
	{
		AddMessage2Log($arFields);
	}
	// создаем обработчик события "OnAfterUserUpdate"
	function OnAfterUserUpdateHandler(&$arFields)
	{
		if($arFields["RESULT"]) {
			AddMessage2Log("Запись пользователя с кодом ".$arFields["ID"]." изменена.");
			AddMessage2Log($arFields);
		}
		else
			AddMessage2Log("Ошибка изменения записи ".$arFields["ID"]." (".$arFields["RESULT_MESSAGE"].").");
	}
}

/*function custom_mail($to,$subject,$body,$headers) {
	$f=fopen($_SERVER["DOCUMENT_ROOT"]."/maillog.txt", "a+");
	fwrite($f, print_r(array('TO' => $to, 'SUBJECT' => $subject, 'BODY' => $body, 'HEADERS' => $headers),1)."\n========\n");
	fclose($f);
	return mail($to,$subject,$body,$headers);
}*/


include_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/wsrubi.smtp/classes/general/wsrubismtp.php");
