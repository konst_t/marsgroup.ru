<?

function AgentGetCurrencyRate()
{
	global $DB;

	// подключаем модуль "валют"
	if(!CModule::IncludeModule('currency'))
		return "AgentGetCurrencyRate();";

	$attempt = 0;
	$mAnswer = False;
	$rateDay = GetTime(time(), "SHORT", LANGUAGE_ID);
	$arCurList = array('USD', 'EUR');

	while (!$mAnswer) {
		$mAnswer = GetCurrencyXML();
		$attempt++;
		if (!$mAnswer && $attempt>100) {
			mail('tazin@marsgroup.ru', 'Неудача обновления курсов валют', 'Неудача обновления курсов валют',"Content-type: text/html; charset=\"utf-8\" \r\n");
			break;
		}
	}

	if ($mAnswer) {
		require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/xml.php");
		// не работает в php7
		//$strQueryText = eregi_replace("<!DOCTYPE[^>]{1,}>", "", $strQueryText);
		//$strQueryText = eregi_replace("<"."\?XML[^>]{1,}\?".">", "", $strQueryText);
		$strQueryText =  preg_replace("(<!DOCTYPE[^>]{1,}>)i", "", $mAnswer);
		$strQueryText =  preg_replace("(<"."\?XML[^>]{1,}\?".">)i", "", $strQueryText);
		$objXML = new CDataXML();
		$objXML->LoadString($strQueryText);
		$arData = $objXML->GetArray();
		$arFields = array();
		$arCurRate["CURRENCY_CBRF"] = array();

		if (is_array($arData) && count($arData["ValCurs"]["#"]["Valute"])>0) {
			for ($j1 = 0; $j1<count($arData["ValCurs"]["#"]["Valute"]); $j1++) { 
				if (in_array($arData["ValCurs"]["#"]["Valute"][$j1]["#"]["CharCode"][0]["#"], $arCurList)) { 
					CCurrencyRates::Add(array(
						'CURRENCY' => $arData["ValCurs"]["#"]["Valute"][$j1]["#"]["CharCode"][0]["#"],
						'DATE_RATE' => $rateDay,
						'RATE' => DoubleVal(str_replace(",", ".", $arData["ValCurs"]["#"]["Valute"][$j1]["#"]["Value"][0]["#"])),
						'RATE_CNT' => IntVal($arData["ValCurs"]["#"]["Valute"][$j1]["#"]["Nominal"][0]["#"]),
					));
				}
			}
		}
	}

	return "AgentGetCurrencyRate();";
}

function GetCurrencyXML() {
	global $DB;
	$rateDay = GetTime(time(), "SHORT", LANGUAGE_ID);
	$QUERY_STR = "date_req=".$DB->FormatDate($rateDay, CLang::GetDateFormat("SHORT", SITE_ID), "D.M.Y");
	$strQueryText = QueryGetData("www.cbr.ru", 80, "/scripts/XML_daily.asp", $QUERY_STR, $errno, $errstr);
	$strQueryText = trim($strQueryText);
	if (strlen($strQueryText) <= 0) {
		AddMessage2Log("Пустой ответ ЦБ");
		return false;
	}
	// данная строка нужна только если у вас сайт в кодировке utf8
	$strQueryText = iconv('windows-1251', 'utf-8', $strQueryText); 
	return $strQueryText;
}

?>