<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Новинки");
?><section class="sale">
	<div class="sale_container container clearfix">

		<h1>НОВИНКИ</h1>

<?
$GLOBALS['newFilter'] = array('PROPERTY_NOVINKA_VALUE' => "Новинка");

$section = htmlspecialchars($_GET["sect"]);

$arIBlockSection = GetIBlockSection($section);
$sections = GetIBlockSectionList(2, false, Array(), false, Array("LEFT_MARGIN"=>$arIBlockSection["LEFT_MARGIN"], "RIGHT_MARGIN"=>$arIBlockSection["RIGHT_MARGIN"]));
while($sec = $sections->GetNext()){
	$GLOBALS['newFilter']['SECTION_ID'][] = $sec['ID'];
}

$chain = [];
$nav = CIBlockSection::GetNavChain(2, $section ,array('ID'));
while($chain_ = $nav->GetNext()){
	$chain[] = $chain_['ID'];
}
//echo "<pre>"; print_r($chain); echo "</pre>";


$arFilter = array("IBLOCK_ID"=>2,"DEPTH_LEVEL"=>1,"ACTIVE"=>"Y");
$obj = CIBlockSection::GetList(Array("left_margin"=>"asc"), $arFilter, false, array('ID', 'NAME', 'DEPTH_LEVEL'));
$arSectTmp = array();
while($arList = $obj->GetNext())
{                                                                       //   echo "<pre>"; print_r($arList); echo "</pre>";
	$arSectTmp[$arList['ID']]['ID'] = $arList['ID'];
	$arSectTmp[$arList['ID']]['NAME'] = $arList['NAME'];
	if($arList["ID"] == $section || in_array($arList["ID"], $chain)){
		$arSectTmp[$arList['ID']]["CURRENT"]="Y";
	}else{
		$arSectTmp[$arList['ID']]["CURRENT"]="N";
	}
	$arFilter2 = array("IBLOCK_ID"=>2,"SECTION_ID"=>$arList['ID'],"ACTIVE"=>"Y");
	$obj2 = CIBlockSection::GetList(Array("left_margin"=>"asc"), $arFilter2,false,array('ID', 'NAME', 'DEPTH_LEVEL'));
	$arSectTmp2 = array();
	while($arList2 = $obj2->GetNext())
	{
		$arSectTmp2[$arList2['ID']]['ID'] = $arList2['ID'];
		$arSectTmp2[$arList2['ID']]['NAME'] = $arList2['NAME'];
		if($arList2["ID"] == $section || in_array($arList2["ID"], $chain)){
			$arSectTmp2[$arList2['ID']]["CURRENT"]="Y";
		}else{
			$arSectTmp2[$arList2['ID']]["CURRENT"]="N";
		}
		$arFilter3 = array("IBLOCK_ID"=>2,"SECTION_ID"=>$arList2['ID'],"ACTIVE"=>"Y");
		$obj3 = CIBlockSection::GetList(Array("left_margin"=>"asc"), $arFilter3,false,array('ID', 'NAME', 'DEPTH_LEVEL'));
		$arSectTmp3 = array();
		while($arList3 = $obj3->GetNext())
		{
			$arSectTmp3[$arList3['ID']]['ID'] = $arList3['ID'];
			$arSectTmp3[$arList3['ID']]['NAME'] = $arList3['NAME'];
			if($arList3["ID"] == $section || in_array($arList3["ID"], $chain)){
				$arSectTmp3[$arList3['ID']]["CURRENT"]="Y";
			}else{
				$arSectTmp3[$arList3['ID']]["CURRENT"]="N";
			}
		}
		if (count($arSectTmp3)) {
			$arSectTmp2[$arList2['ID']]['IS_PARENT'] = 'Y';
			$arSectTmp2[$arList2['ID']]['SUBCAT'] = $arSectTmp3;
		}else{
			$arSectTmp2[$arList2['ID']]['IS_PARENT'] = 'N';
		}
	}
	$arSectTmp[$arList['ID']]['SUBCAT'] = $arSectTmp2;
	if (count($arSectTmp2)) {
		$arSectTmp[$arList['ID']]['IS_PARENT'] = 'Y';
	}else{
		$arSectTmp[$arList['ID']]['IS_PARENT'] = 'N';
	}
}
$catalog = $arSectTmp;

?>
<section class="catalog_sect clearfix">
	<div class="catalog_sect_container container">
		<aside class="catalog_sect_filter">

		<div class="filter_block">
			<div class="filter_block_head open">Категории</div>
			<div class="filter_block_body" data-role="bx_filter_block">
				<ul class="cat_left_menu">
					<? foreach($catalog as $sect){ ?>
					<li class="<? if($sect['CURRENT'] == 'Y'){ ?>menu_current<?}?> <? if($sect['CURRENT'] == 'Y'){ ?>open_sub<?}?>">
						<a href="/new/?sect=<?=$sect['ID'];?>"><?=$sect['NAME'];?></a>
						<? if(count($sect['SUBCAT'])){ ?>
						<i class="fa fa-angle-<? if($sect['CURRENT'] == 'Y'){ echo 'up';}else{ echo 'down';}?> cat_left_icon" aria-hidden="true"></i>
						<ul class="cat_left_submenu" >
							<? foreach($sect['SUBCAT'] as $sect2){ ?>
							<li class="<? if($sect2['CURRENT'] == 'Y'){ ?>menu_current<?}?> <? if($sect2['CURRENT'] == 'Y'){ ?>open_sub<?}?>">
								<a href="/new/?sect=<?=$sect2['ID'];?>"><?=$sect2['NAME'];?></a>
								<? if(isset($sect2['SUBCAT'])){ ?>
								<i class="fa fa-angle-<? if($sect['CURRENT'] == 'Y'){ echo 'up';}else{ echo 'down';}?> cat_left_icon" aria-hidden="true"></i>
								<ul class="cat_left_submenu">
									<? foreach($sect2['SUBCAT'] as $sect3){ ?>
									<li <? if($sect3['CURRENT'] == 'Y'){ ?>class="menu_current"<?}?>>
										<a href="/new/?sect=<?=$sect3['ID'];?>"><?=$sect3['NAME'];?></a>
										
									</li>
									<?}?>
								</ul>
								<?}?>
							</li>
							<?}?>
						</ul>
						<?}?>
					</li>
					<?}?>
				</ul>
			</div>
		</div>

		</aside>


		<div class="catalog_sect_list">

<?
$APPLICATION->IncludeComponent(
	"bitrix:catalog.section", 
	".default", 
	array(
		"ACTION_VARIABLE" => "action",
		"ADD_PICT_PROP" => "-",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"ADD_TO_BASKET_ACTION" => "ADD",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BACKGROUND_IMAGE" => "-",
		"BASKET_URL" => "/personal/cart/",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPATIBLE_MODE" => "Y",
		"CONVERT_CURRENCY" => "Y",
		"CURRENCY_ID" => "RUB",
		"CUSTOM_FILTER" => "{\"CLASS_ID\":\"CondGroup\",\"DATA\":{\"All\":\"AND\",\"True\":\"True\"},\"CHILDREN\":[]}",
		"DETAIL_URL" => "",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_COMPARE" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_ORDER2" => "desc",
		"ENLARGE_PRODUCT" => "STRICT",
		"FILTER_NAME" => "newFilter",
		"HIDE_NOT_AVAILABLE" => "N",
		"HIDE_NOT_AVAILABLE_OFFERS" => "N",
		"IBLOCK_ID" => "2",
		"IBLOCK_TYPE" => "catalog",
		"INCLUDE_SUBSECTIONS" => "Y",
		"LABEL_PROP" => array(
			0 => "NOVINKA",
		),
		"LABEL_PROP_MOBILE" => array(
			0 => "NOVINKA",
		),
		"LABEL_PROP_POSITION" => "top-right",
		"LAZY_LOAD" => "N",
		"LINE_ELEMENT_COUNT" => "3",
		"LOAD_ON_SCROLL" => "N",
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"OFFERS_CART_PROPERTIES" => array(
		),
		"OFFERS_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"OFFERS_LIMIT" => "0",
		"OFFERS_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER" => "asc",
		"OFFERS_SORT_ORDER2" => "desc",
		"OFFER_ADD_PICT_PROP" => "-",
		"OFFER_TREE_PROPS" => array(
		),
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "visual",
		"PAGER_TITLE" => "Товары",
		"PAGE_ELEMENT_COUNT" => "16",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons,compare",
		"PRODUCT_DISPLAY_MODE" => "Y",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPERTIES" => array(
		),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'3','BIG_DATA':false},{'VARIANT':'3','BIG_DATA':false},{'VARIANT':'3','BIG_DATA':false},{'VARIANT':'3','BIG_DATA':false}]",
		"PRODUCT_SUBSCRIPTION" => "N",
		"PROPERTY_CODE" => array(
			0 => "NOVINKA",
			1 => "",
		),
		"PROPERTY_CODE_MOBILE" => array(
			0 => "NOVINKA",
		),
		"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
		"RCM_TYPE" => "personal",
		"SECTION_CODE" => "",
		"SECTION_ID" => "",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SEF_MODE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SHOW_ALL_WO_SECTION" => "Y",
		"SHOW_CLOSE_POPUP" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_FROM_SECTION" => "N",
		"SHOW_MAX_QUANTITY" => "N",
		"SHOW_OLD_PRICE" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"SHOW_SLIDER" => "N",
		"SLIDER_INTERVAL" => "3000",
		"SLIDER_PROGRESS" => "N",
		"TEMPLATE_THEME" => "blue",
		"USE_ENHANCED_ECOMMERCE" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"COMPONENT_TEMPLATE" => ".default",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);?>

		</div></div>

</div></section>

<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>