<?
if (isset($_GET['change_password']) && $_GET['change_password'] == 'yes') { 
	define("NEED_AUTH", true);
}
	
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php"); 
$APPLICATION->SetTitle("Авторизация");
if ($USER->IsAuthorized()) {
	LocalRedirect('/personal/');
}

$userName = CUser::GetFullName();
if (!$userName)
	$userName = CUser::GetLogin();
?>
<section class="auth">
	<div class="auth_container container">
		<h1 class="auth_h1">Вход</h1>
		<ul class="auth_select">
			<li class="active">Зарегистрированный клиент</li>
			<li>Новый клиент</li>
		</ul>
		<div class="auth_tab auth_in clearfix active">
			<?$APPLICATION->IncludeComponent(
	"bitrix:system.auth.form", 
	"mars", 
	array(
		"FORGOT_PASSWORD_URL" => "/auth/restore.php",
		"PROFILE_URL" => "/personal/",
		"REGISTER_URL" => "",
		"SHOW_ERRORS" => "Y",
		"COMPONENT_TEMPLATE" => "mars"
	),
	false
);?>
		</div>
		<div class="auth_tab auth_reg clearfix ">
			<form class="register_form">
				<div class="reg_error"></div>
				<div class="auth_tab_l clearfix">
					<h3 class="auth_mob_head">Регистрация</h3>
					<div class="r_field">* Необходимые поля</div>
					<label>логин *</label>
					<input required type="text" name="login">
					<label>пароль *</label>
					<input required type="password" name="pass">
					<label>ФИО *</label>
					<input required type="text" name="name">
					<label>контактный телефон *</label>
					<input required type="tel" name="tel">
					<label>e-mail *</label>
					<input required type="email" name="email">
					<div class="auth_tab_checks">					
						<div class="vid_label">вид деятельности *</div>
						<input type="checkbox" id="f1" name="vid[]" value="розничный магазин">
						<label for="f1">розничный магазин</label>
						<input type="checkbox" id="f2" name="vid[]" value="оптовый магазин">
						<label for="f2">оптовый магазин</label>
						<input type="checkbox" id="f3" name="vid[]" value="интернет-магазин">
						<label for="f3">интернет-магазин</label>
					</div>
				</div>
				<div class="auth_tab_r clearfix">
					<label>название компании/ип *</label>
					<input required type="text" name="company">
					<label>ИНН *</label>
					<input required type="text" name="inn">
					<label>область/край *</label>
					<input required type="text" name="oblast">
					<label>город *</label>
					<input required type="text" name="gorod">
					<label>адрес магазина, офиса, склада *</label>
					<input required type="text" name="adres">					
					<label>какие товары вас заинтересовали? *</label>
					<input required type="text" name="products">
					<a class="auth_butt reg_butt" href="javascript:void(0)">Регистрация</a>
				</div>
				
			</form>
		</div>
	</div>
	<!-- /.auth_container container -->
</section>
<!-- /.auth -->

<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>