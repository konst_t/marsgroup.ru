<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Товары из Франции");
?><p>
</p>
<h4> <b><span style="color: #ed008c;"><span style="color: #ed008c;"><a href="https://www.marsgroup.ru/catalog/?q=Tineo"><span style="color: #ed008c;">Tin</span></a></span></span></b><span style="color: #ed008c;">é</span><a href="https://www.marsgroup.ru/catalog/?q=Tineo"><span style="color: #ed008c;">o</span></a></h4>
<span style="color: #ed008c;"> </span>
<p>
 <a href="https://www.marsgroup.ru/catalog/?q=tineo"><img width="307" alt="tineo.jpg" src="/upload/medialibrary/2f6/tineo.jpg" height="129" title="tineo.jpg"></a><br>
</p>
<p>
	 Семейное предприятие появилось в 1978 году на западе Франции в городе Анже и за 40 лет стало важным игроком на рынке детских товаров. Основатели, увлеченные темой ухода за детьми, начали с изготовления матрасов для малышей и с годами пополнили ассортимент текстилем и целой гаммой детских продуктов и аксессуаров для сна, отдыха и путешествий. Своего нынешнего положения предприятие достигло в 1990-е годы, став маркой компании Candide Baby Group и получив название P'tit Dodo, а с 2012 — Tinéo.&nbsp; Сегодня Tinéo предлагает умные и безопасные продукты и дает семьям то, что в чем они нуждаются.
</p>
<p>
	 &nbsp;
</p>
<h4> <b><span style="color: #ed008c;"><a href="https://www.marsgroup.ru/catalog/?q=Candide"><span style="color: #ed008c;">Candide</span></a></span></b> </h4>
<p>
 <a href="https://www.marsgroup.ru/catalog/?q=Candide"><img width="307" alt="candide.jpg" src="/upload/medialibrary/33a/candide.jpg" height="201" title="candide.jpg"></a><br>
</p>
<p>
</p>
<p>
	 Candide является производителем и дистрибьютором товаров в области ухода за детьми и игрушек. Более 30 лет компания ежегодно предлагает оригинальные коллекции инновационных продуктов для сна, отдыха и кормления. Посвященная исключительно миру ребенка, его комфорту и комфорту мамы, продукция Candide является качественной и абсолютно безопасной. Специалисты сотрудничают с профессионалами по заботе о ребенке, постоянно следят за тенденциями в меняющемся мире и ориентируются на потребности родителей и тех, кто собирается ими стать.&nbsp;
</p>
<p>
	 &nbsp;
</p>
<p>
 <br>
</p>
<h4> <b><span style="color: #ed008c;"><a href="https://www.marsgroup.ru/catalog/?q=Combelle"><span style="color: #ed008c;">Combelle</span></a></span></b> </h4>
<p>
 <a href="https://www.marsgroup.ru/catalog/?q=Combelle"><img width="307" alt="combelle.jpg" src="/upload/medialibrary/d35/combelle.jpg" height="51" title="combelle.jpg"></a><br>
</p>
<p>
	 История компании Combelle началась в 1926 году с Марселя Комбэлля, который изготавливал ручки для зонтов по знаменитой технологии гнутья древесины. Это уникальное мастерство гнуть дерево передавалось из поколения в поколение, и сегодня наследники компании следуют по стопам ее основателя с той же страстью и энтузиазмом. В 2015 году Combelle присоединилась к группе Candide Baby Group, специализирующейся на производстве товаров по уходу за детьми с брендами Candide, Tineo и Latitude Child.
</p>
<p>
</p>
<p>
	 В мастерских компании можно прочитать фразу «Из дерева рождается форма» — простые слова, которые выражают любовь к материалу. Вся детская мебель изготавливается из массива бука — из деревьев старше 80 лет, тщательно отобранных во французском регионе Овернь.&nbsp;
</p>
<p>
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>